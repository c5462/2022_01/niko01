pageextension 50106 MMCompaniesEXTC01 extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(MMClientesC01; ClientesF(False))
            {
                ApplicationArea = All;
                Caption = 'Nº clientes';
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ClientesF(true);
                end;
            }
            field(MMProveedoresC01; ProveedoresF(False))
            {
                ApplicationArea = All;
                Caption = 'Nº proveedores';
                trigger OnDrillDown()
                begin
                    ProveedoresF(true);
                end;
            }
            field(MMCuentasC01; CuentasF(False))
            {
                ApplicationArea = All;
                Caption = 'Nº cuentas';
                trigger OnDrillDown()
                begin
                    CuentasF(true);
                end;
            }
            field(MMProductosC01; ProductosF(False))
            {
                ApplicationArea = All;
                Caption = 'Nº productos';
                trigger OnDrillDown()
                begin
                    ProductosF(True);
                end;
            }


        }

    }
    actions
    {
        addlast(processing)
        {
            action(MMCopiaSeguridad)
            {
                ApplicationArea = All;
                Caption = 'Copia de seguridad';
                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;
            }
        }
        addlast(processing)
        {
            action(MMCopiaSeguridad2)
            {
                ApplicationArea = All;
                Caption = 'Restaurar Copia de seguridad';
                trigger OnAction()
                begin
                    RestaurarCopiasF();
                end;
            }
        }
    }

    local procedure ClientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        exit(DatosTablaF(rlCustomer, pDrillDown));
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer

    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillDown));


    end;

    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillDown));
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
    begin
        exit(DatosTablaF(rlItem, pDrillDown));
    end;

    local procedure EjemploRecordRefsF()
    var
        xlRecRef: RecordRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    xlRecRef.FieldIndex(i).Value;
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                xlRecRef.Field(4).Validate('');
            until xlRecRef.Next() = 0;
        end;

        // xlRecRef.KeyIndex(1).FieldCount();

        //VIEJO SISTEMA. 

    end;

    local procedure DatosTablaF(pRecVariant: Variant; pDrillDown: Boolean): Integer
    var
        xlRecRef: RecordRef;
        rlCompany: Record Company;
    begin
        if pRecVariant.IsRecord then begin
            xlRecRef.GetTable(pRecVariant);
        end;
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                xlRecRef.ChangeCompany(Rec.Name);
            end;
            if pDrillDown then begin
                page.Run(0, pRecVariant);
            end else begin
                exit(xlRecRef.Count);
            end;
        end;
    end;

    local procedure CopiaSeguridadF()
    var
        rlCompany: Record Company;
        rlAllObjWithCaption: Record AllObjWithCaption;
        xlTextBuilder: TextBuilder;
        xlRecRef: RecordRef;
        culTypeHelper: Codeunit "Type Helper";
        i: Integer;
        xlNombreFichero: Text;
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        xlInStr: Instream;
        xlOutStr: OutStream;
    begin
        CurrPage.SetSelectionFilter(rlCompany);
        if rlCompany.FindSet(false) then begin
            repeat
                xlTextBuilder.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                //Escribir en el fichero
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::Customer, Database::"G/L Account", Database::Vendor, Database::Item);
                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        xlTextBuilder.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                xlTextBuilder.Append('IR <' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                for i := 1 to xlRecRef.FieldCount do begin
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append('<' + xlRecRef.FieldIndex(i).Name + '> · <' + Format(xlRecRef.FieldIndex(i).Value) + '>' + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                    // xlRecRef.FieldIndex(i).Value;
                                end;
                                xlTextBuilder.Append('FR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        xlRecRef.Close();
                        xlTextBuilder.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;

                //Bucle de tablas
                xlTextBuilder.Append('FE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
            until rlCompany.Next() = 0;
            TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            xlOutStr.WriteText(xlTextBuilder.ToText());
            xlNombreFichero := 'CopiaDatos.csv';
            TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                Error('Error 404');
            end;
        end;






    end;

    local procedure ValorCampoF(pFieldRef: FieldRef; pValor: Text): Text
    begin
        if pFieldRef.Type = pFieldRef.Type::Option then begin

        end else begin
            exit(pValor);
        end;
    end;

    local procedure RestaurarCopiasF()
    var
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        xlInStr: Instream;
        xlLinea: Text;
        rlCustomer: Record Customer;
        clSeparador2: Label '·', Locked = true;
        rlField: Record Field;
        xlRecordRef: RecordRef;
    begin

        //1)Subir el fichero de copias de seguridad 
        //2)Recorrer el fichero
        //3) Si es inicio de tabla. Abrir el registro 
        //4) Si es fin de tabla. Cerrar el registro
        //5) Si es inicio de registro, crearemos el registro
        //6) Si es fin de registro, modificar registro
        //7) Por cada campo, asignar su valor
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin
                xlInStr.ReadText(xlLinea);
                case true of
                    CopyStr(xlLinea, 1, 3) = 'IT<':
                        begin

                            xlRecordRef.Open(NumeroTablaF(CopyStr(xlLinea, 4).TrimEnd('>')));

                        end;
                    CopyStr(xlLinea, 1, 3) = 'IR<':
                        begin
                            xlRecordRef.Init();
                        end;
                    CopyStr(xlLinea, 1, 3) = 'FT<':
                        begin
                            xlRecordRef.Close();
                        end;
                    CopyStr(xlLinea, 1, 3) = 'FR<':
                        begin
                            xlRecordRef.Insert(false);

                        end;
                    xlLinea.StartsWith('<'):
                        begin

                            if (xlRecordRef.Field(NumCampoF(xlRecordRef.Number, xlLinea.Split(clSeparador2).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal) then begin
                                xlRecordRef.Field(NumCampoF(xlRecordRef.Number, xlLinea.Split(clSeparador2).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))).Value := CampoYValorF(xlRecordRef.Field(NumCampoF(xlRecordRef.Number, xlLinea.Split(clSeparador2).Get(1).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'))), xlLinea.Split(clSeparador2).Get(2).TrimStart(' ').TrimEnd(' ').TrimStart('<').TrimEnd('>'));
                            end;

                        end;
                end;

            end;

        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1)', GetLastErrorText());
        end;
    end;

    local procedure NumeroTablaF(pNombre: Text): Integer
    var
        rlAllObjWithCaption: Record AllObjWithCaption;

    begin
        rlAllObjWithCaption.SetRange("Object Name", pNombre);
        rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
        rlAllObjWithCaption.SetLoadFields("Object ID");
        if rlAllObjWithCaption.FindFirst() then begin
            exit(rlAllObjWithCaption."Object ID");
        end;
    end;

    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;

    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        rlCustomer: Record Customer;
        xlBigInteger: BigInteger;
        rlField: Record Field;
        rlItem: Record Item;
        i: Integer;
        xlBoolean: Boolean;
        xlDecimal: Decimal;
        xlDate: Date;
        rlMMConfiguracionC01: Record MMConfiguracionC01;
        xlGuid: Guid;
        xlDateTime: DateTime;
        xlTime: Time;
    begin
        case pFieldRef.Type of
            pFieldRef.Type::BigInteger, pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.Type::Boolean:
                if Evaluate(xlBoolean, pValor) then begin
                    exit(xlBoolean)
                end;
            pFieldRef.Type::Decimal:
                if Evaluate(xlDecimal, pValor) then begin
                    exit(xlDecimal)
                end;
            pFieldRef.Type::Date:
                if Evaluate(xlDate, pValor) then begin
                    exit(xlDate)
                end;
            pFieldRef.Type::Blob:
                exit(rlMMConfiguracionC01.MiBlob);
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::DateTime:
                if Evaluate(xlDateTime, pValor) then begin
                    exit(xlDateTime)
                end;
            pFieldRef.Type::Media:
                exit(rlCustomer.Image);
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            pFieldRef.Type::MediaSet:
                exit(rlItem.Picture);

            else
                exit(pValor);
        end

    end;

}
