﻿$VersionBC = '180';
$Instancia = 'BC'+$VersionBC;
$NombreApp='Curso01'
$Version='1.20220118.0.0'
$App1 = 'C:\Users\PC\Documents\AL\Curso01\MM_'+$NombreApp+'_'+$Version+'.app'


Set-ExecutionPolicy RemoteSigned;

Import-Module "C:\Program Files\Microsoft Dynamics 365 Business Central\$VersionBC\Service\NavAdminTool.ps1"

if ($VersionBC -lt '160') {
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Model.Tools.psd1"
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
}

Uninstall-NAVApp -Path $App1 -ServerInstance $Instancia -Force
Unpublish-NAVApp -Path $App1 -ServerInstance $Instancia
#Sync-NAVApp -ServerInstance $Instancia -Name $NombreApp -Force -Mode Clean
Get-NAVAppInfo -ServerInstance $Instancia -Tenant default -TenantSpecificProperties | Where Publisher -eq 'MM'

#Unpublish-NAVApp -Path $App1 -ServerInstance $Instancia -Version $Version
