pageextension 50105 MMSalesOrderListExt extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(MMExportarPedidosVenta)
            {
                ApplicationArea = All;
                Caption = 'Exportar Pedidos de Venta';
                trigger OnAction()
                begin
                    ExportarPedidosVentaF();

                end;
            }
            action(MMImportarPedidosVenta)
            {
                ApplicationArea = All;
                Caption = 'Importar Pedidos de Venta';
                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
        }
    }

    procedure ExportarPedidosVentaF()
    var

        pglSalesOrderList: Page "Sales Order List";
        xlOutStr: OutStream;
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        rlMMLinPlanVacunacionC01: Record "Sales Line";
        rlSalesHeader: Record "Sales Header";
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        culTypeHelper: Codeunit "Type Helper";
        xlSeparador: Label '·', Locked = true;
    begin
        CurrPage.SetSelectionFilter(rlSalesHeader);
        //SetLoadFields()
        if rlSalesHeader.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
            TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
            repeat
                xlOutStr.WriteText('C');
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Document Type"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Name");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Contact");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Bill-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(rlSalesHeader."Sell-to Customer No.");
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Status"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Posting Date"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Due Date"));
                xlOutStr.WriteText(xlSeparador);
                xlOutStr.WriteText(Format(rlSalesHeader."Shipment Date"));
                xlOutStr.WriteText();
                //RECORREMOS LAS LÍNEAS
                rlMMLinPlanVacunacionC01.SetRange("Document No.", rlSalesHeader."No.");
                rlMMLinPlanVacunacionC01.SetRange("Document Type", rlSalesHeader."Document Type");
                if rlMMLinPlanVacunacionC01.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                    //1 p: campo por el que queremos filtrar el parametro. 2p: "DESDE" | VALOR, si no pongo el 3, estoy diciendo que filtrame el campo 1 con este VALOR. Si le ponemos el 3p: "HASTA".
                    //SETFILTER. 3 valores. Primero: Campo. Segundo: Cadena. Tecero: Valores.
                    repeat
                        xlOutStr.WriteText('L');
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Line No."));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Document No."));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Document Type"));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01."Type"));
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlMMLinPlanVacunacionC01."No.");
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(rlMMLinPlanVacunacionC01.Description);
                        xlOutStr.WriteText(xlSeparador);
                        xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01.Amount));
                        xlOutStr.WriteText()
                    until rlMMLinPlanVacunacionC01.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;



        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        xlNombreFichero := 'PedidosVentas.csv';
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Error 404');
        end;

    end;

    local procedure ImportarPedidosVentaF()
    var
        xlInStr: Instream;
        xlLinea: Text;
        rlSalesHeader: Record "Sales Header";
        clMMCapturarErroresC01: Codeunit MMCapturarErroresC01;
        rlSalesLine: Record "Sales Line";
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        clSeparador: Label '·', Locked = true;
    begin
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStr.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlSalesHeader.Init();
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 3, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Document Type" de la cabecera. Valor del campo incorrecto: %1', xlLinea[3]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 2, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "No." de la cabecera. Valor del campo incorrecto: %1', xlLinea[2]));
                            end;
                            rlSalesHeader.Insert(true);
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 7, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Sell-to Customer No." de la cabecera. Valor del campo incorrecto: %1', xlLinea[7]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 6, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Bill-to Customer No." de la cabecera . Valor del campo incorrecto: %1', xlLinea[6]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 4, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Bill-to Name" de la cabecera . Valor del campo incorrecto: %1', xlLinea[4]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 5, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Bill-to Contact" de la cabecera . Valor del campo incorrecto: %1', xlLinea[5]));
                            end;

                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 8, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Status" de la cabecera. Valor del campo incorrecto: %1', xlLinea[8]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 9, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Posting Date" de la cabecera . Valor del campo incorrecto: %1', xlLinea[9]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 10, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Due Date" de la cabecera. Valor del campo incorrecto: %1', xlLinea[10]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesHeader, 11, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Shipment Date" de la cabecera. Valor del campo incorrecto: %1', xlLinea[11]));
                            end;

                            rlSalesHeader.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        begin
                            rlSalesLine.Init();
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 4, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Document Type" de la línea. Valor del campo incorrecto: %1', xlLinea[4]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 3, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Document No." de la línea. Valor del campo incorrecto: %1', xlLinea[3]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 2, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Line No." de la línea. Valor del campo incorrecto: %1', xlLinea[2]));
                            end;
                            rlSalesLine.Insert(true);
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 5, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Type" de la línea. Valor del campo incorrecto: %1', xlLinea[5]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 6, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "No." de la línea. Valor del campo incorrecto: %1', xlLinea[6]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 7, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Description" de la línea. Valor del campo incorrecto: %1', xlLinea[7]));
                            end;
                            if not clMMCapturarErroresC01.CapturaValidacionesF(rlSalesLine, 8, xlLinea, clSeparador) then begin
                                InsertaMensajeErrorF(StrSubstNo('Error en el campo "Amount" de la línea. Valor del campo incorrecto: %1', xlLinea[8]));
                            end;

                            rlSalesLine.Modify(true);

                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
            /*
            if not TempLMMLogC01.IsEmpty then begin
                Message('Revise incidencias importación');
                Page.Run(0,TempLMMLogC01)
            end;
            */
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;

    local procedure InsertaMensajeErrorF(pMensaje: Text)
    var
        rlMMLogC01: Record MMLogC01 temporary;
    begin
        rlMMLogC01.Init();
        rlMMLogC01.Insert(true);
        rlMMLogC01.Validate(Mensaje, pMensaje);
        rlMMLogC01.Modify(true);
    end;


    /*
    xlFecha: Date;
        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
        xlStatus: Enum "Sales Document Status";
        Integer 
        Code
    */
}
