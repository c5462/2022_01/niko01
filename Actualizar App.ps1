﻿$VersionBC= '190'
$Instancia = 'BC' + $VersionBC
$Ruta = 'C:\AL\'
$NombreApp = 'CursoTardes01'
$VersionAPP = '1.0.0.1'
$OldVersion = '1.0.0.0'
$App1 = $Ruta + 'Default publisher_' + $NombreApp + '_' + $VersionAPP + '.app'
$User = 'Varios'
$Rol = 'SUPER'
$Pass = 'Curs0Tecon01@'

 

Set-ExecutionPolicy RemoteSigned

 

Import-Module "C:\Program Files\Microsoft Dynamics 365 Business Central\$VersionBC\Service\NavAdminTool.ps1"
if ($VersionBC -lt '160') {
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Model.Tools.psd1"
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
}

 

Publish-NAVApp -Path $App1 -SkipVerification -ServerInstance $Instancia
Sync-NAVApp -Name $NombreApp -ServerInstance $Instancia -Force -Version $VersionAPP
Start-NAVAppDataUpgrade -ServerInstance $Instancia -Name $NombreApp -Version $VersionAPP
Uninstall-NAVApp -ServerInstance $Instancia -Name $NombreApp -Version $OldVersion
UnPublish-NAVApp -ServerInstance $Instancia -Name $NombreApp -Version $OldVersion

 


Get-NAVAppInfo -ServerInstance $Instancia -Tenant default -TenantSpecificProperties | Where Publisher -eq 'TCN'