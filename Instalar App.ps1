﻿$VersionBC = '180';
$Instancia = 'BC'+$VersionBC;
$NombreApp='Curso01'
$App1 = 'C:\Users\PC\Documents\AL\Curso01\MM_'+$NombreApp+'_1.20220118.0.0.app'


Set-ExecutionPolicy RemoteSigned;

Import-Module "C:\Program Files\Microsoft Dynamics 365 Business Central\$VersionBC\Service\NavAdminTool.ps1"

if ($VersionBC -lt '160') {
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Model.Tools.psd1"
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
}

Publish-NAVApp -Path $App1 -SkipVerification -ServerInstance $Instancia

Sync-NAVTenant -ServerInstance $Instancia -Tenant Default -force  #-Mode ForceSync  
Sync-NAVApp -Name $NombreApp -ServerInstance $Instancia -Force
Install-NAVApp -Path $App1 -ServerInstance $Instancia -Force

Get-NAVAppInfo -ServerInstance $Instancia -Tenant default -TenantSpecificProperties | Where Publisher -eq 'MM'