﻿$VersionBC= '190'
$Instancia = 'BC' + $VersionBC
$Ruta = 'C:\AL\'
$User = 'Varios'
$Rol = 'SUPER'
$Pass = 'Curs0Tecon01@'

 

Set-ExecutionPolicy RemoteSigned

 

Import-Module "C:\Program Files\Microsoft Dynamics 365 Business Central\$VersionBC\Service\NavAdminTool.ps1"
if ($VersionBC -lt '160') {
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Model.Tools.psd1"
    Import-Module "C:\Program Files (x86)\Microsoft Dynamics 365 Business Central\$VersionBC\RoleTailored Client\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
}

 


# Windows Autentication
New-NAVServerUser -ServerInstance $Instancia -WindowsAccount $(whoami)
New-NAVServerUserPermissionSet -ServerInstance $Instancia -WindowsAccount $(whoami) -PermissionSetId $Rol

 

# UserPassword
New-NAVServerUser -ServerInstance $Instancia -UserName $User -Password (ConvertTo-SecureString $Pass -AsPlainText -Force)
New-NAVServerUserPermissionSet -ServerInstance $Instancia -UserName $User -PermissionSetId $Rol