pageextension 50104 MMVendorCardExtC01 extends "Vendor Card"
{
    actions
    {
        addlast(processing)
        {
            action(MMCrearCliente)
            {
                ApplicationArea = All;
                Caption = 'Crear proveedor como cliente';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                begin
                    if rlCustomer.Get(Rec."No.") then begin
                        if Confirm('¿Quiere sobreescribir el cliente?', false) then begin
                            rlCustomer.Init();
                            rlCustomer.TransferFields(Rec, true, true);
                            rlCustomer.Modify(true);
                            Message('Datos actualizados');
                        end else begin
                            Error('Proceso cancelado');
                        end;
                    end else begin
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                        Message('Proveedor creado como cliente');
                    end;
                end;

            }
        }
    }
}
