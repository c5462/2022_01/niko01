pageextension 50103 "MMClientesC01" extends "Vendor List"
{
    actions
    {
        addlast(processing)
        {
            action(MMImprimirFacturaC01)
            {
                ApplicationArea = All;
                trigger OnAction()
                var
                    myInt: Integer;
                begin
                    rSalesShipmentInvoice.SetRange("No.", '193040');
                    repGenericoVentas.SetTableView(rSalesShipmentInvoice);
                    repGenericoVentas.Run();
                end;
            }
            action(MMImprimirAlbaranC01)
            {
                ApplicationArea = All;
                trigger OnAction()
                var
                    myInt: Integer;
                begin
                    rSalesShipmentHeader.SetRange("No.", '102056');
                    repGenericoVentas.SetTableView(rSalesShipmentHeader);
                    repGenericoVentas.Run();
                end;
            }
        }
    }
    var
        rSalesShipmentHeader: Record "Sales Shipment Header";
        rSalesShipmentInvoice: Record "Sales Invoice Header";
        repGenericoVentas: Report MMGenericoVentasC01;
}
