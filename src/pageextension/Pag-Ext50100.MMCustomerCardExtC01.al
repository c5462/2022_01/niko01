pageextension 50100 "MMCustomerCardExtC01" extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(MMVacunasC01)
            {
                Caption = 'Vacunas';
                field(MMTipoVacunaC01; Rec.MMTipoVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Tipo de vacuna field.';
                    ApplicationArea = All;
                }
                field(MMFechaUltVacunaC01; Rec.MMFechaUltVacunaC01)
                {

                    ToolTip = 'Specifies the value of the Fecha de última vacuna field.';
                    ApplicationArea = All;
                }
                field(MMNumVacunasC01; Rec.MMNumVacunasC01)
                {
                    ToolTip = 'Specifies the value of the Nº de vacunas  field.';
                    ApplicationArea = All;

                }

            }
        }
    }
    actions
    {
        addlast(processing)
        {
            action(MMAsignarBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = 'Asignar vacuna bloqueada';
                trigger OnAction()
                begin
                    Rec.Validate(MMTipoVacunaC01, 'BLOQUEADA');
                end;
            }
            action(MMSwitchVariableC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba variables globales a BC';
                trigger OnAction()
                var
                    xcuMMFuncionesAppC01: Codeunit MMVblesGlobalesAppC01;
                begin
                    Message('Es valor actual es %1. Lo cambiamos a %2', xcuMMFuncionesAppC01.SwitchF(), not xcuMMFuncionesAppC01.SwitchF());
                    xcuMMFuncionesAppC01.SwitchF(xcuMMFuncionesAppC01.SwitchF());
                end;


            }
            action(MMCrearProveedor)
            {
                ApplicationArea = All;
                Caption = 'Crear cliente como proveedor';
                //PARA QUE SALGA EN LA LISTA LA ACCIÓN EN LOS 3 PUNTITOS PUES SE PONEN LAS DOS INSTRUCCIONES DE ABAJO
                Promoted = true;
                Scope = Repeater;
                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                begin
                    if rlVendor.Get(Rec."No.") then begin
                        if Confirm('¿Quiere sobreescribir el proveedor?', false) then begin
                            rlVendor.Init();
                            rlVendor.TransferFields(Rec, true, true);
                            rlVendor.Modify(true);
                            Message('Datos actualizados');
                        end else begin
                            Error('Proceso cancelado');
                        end;
                    end else begin
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        Message('Cliente creado como proveedor');
                    end;
                end;

            }
        }
    }


    var
        xcuMMFuncionesAppC01: Codeunit MMVblesGlobalesAppC01;

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('Desea salir de esta página?', false))
    end;

}
