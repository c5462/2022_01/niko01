pageextension 50101 "MMExtensionPostedSalesC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(MMEtiquetasC01)
            {
                ApplicationArea = all;
                trigger OnAction()
                var
                    replEtiquetas: Report MMEtiquetaC01;
                    rlSalesShipmentLines: Record "Sales Shipment Line";
                begin
                    rlSalesShipmentLines.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLines);
                    replEtiquetas.RunModal();
                end;

            }
        }
    }
}
