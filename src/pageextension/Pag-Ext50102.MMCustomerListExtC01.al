pageextension 50102 "MMCustomerListExtC01" extends "Customer List"
{
    actions
    {
        addlast(General)
        {
            action(MMSwitchVariableC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba variables globales a BC';
                trigger OnAction()

                begin
                    Message('Es valor actual es %1. Lo cambiamos a %2', xcuMMFuncionesAppC01.SwitchF(), not xcuMMFuncionesAppC01.SwitchF());
                    xcuMMFuncionesAppC01.SwitchF(xcuMMFuncionesAppC01.SwitchF());
                end;


            }
            action(MMPruebaCuCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba para capturar errores';
                trigger OnAction()
                var
                    culMMCapturarErroresC01: Codeunit MMCapturarErroresC01;
                begin
                    if culMMCapturarErroresC01.Run() then begin

                        Message('El botón dice que la función se ha ejecutado correctamente');
                    end;
                end;
            }
            action(MMPrueba02CuCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba para capturar errores 2';
                trigger OnAction()
                var
                    culMMCapturarErroresC01: Codeunit MMCapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        if culMMCapturarErroresC01.FuncionParaCapturarErrorF(xlCodProveedor) then begin

                            Message('Se ha creado el proveedor %1', xlCodProveedor);

                        end else begin
                            Message('No se ha podido ejecutar. Motivo %1', GetLastErrorText());
                        end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }
            action(MMBingoC01)
            {
                ApplicationArea = All;
                Caption = 'Bingo';
                trigger OnAction()
                var
                    culMMFuncionesAPPC01: Codeunit MMFuncionesAPPC01;
                begin
                    culMMFuncionesAPPC01.EjemploArrayF();
                end;

            }
            action(MMPruebaFncSegPlanoC01)
            {
                ApplicationArea = All;
                Caption = 'Bingo';
                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    // Codeunit.Run(Codeunit::MMCodeUnitLentaC01);
                    StartSession(xlSesionIniciada, Codeunit::MMCodeUnitLentaC01);
                    Message('Proceso lanzado en segundo plano');
                end;
            }
            action(MMLogsPrimerPlanoC01)
            {
                ApplicationArea = All;
                trigger OnAction()
                var
                    culMMCodeUnitLentaC01: Codeunit MMCodeUnitLentaC01;
                begin
                    culMMCodeUnitLentaC01.Run();
                end;
            }
            action(MMlogsC01)
            {
                ApplicationArea = All;
                Caption = 'Logs';
                Image = Log;
                RunObject = page MMLogsC01;
            }

            action(MMExportar)
            {
                ApplicationArea = All;
                Caption = 'Exportar Cliente';
                trigger OnAction()
                begin
                    ExportarClienteFicheroF();
                end;
            }
            action(MMSumatorio01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 01';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0;
                    end;
                    Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(MMSumatorio02)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 02';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlDetailedCustLedgEntry.CalcSums(Amount); //LO GUARDA EN AMOUNT
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(MMSumatorio03)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 03';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.SetLoadFields(Amount); //TRAER SÓLO EL CAMPO AMOUNT DEL SELECT. SIEMPRE Y CUANDO NO VAYAMOS A MODIFICAR LA BASE DE DATOS, ES DECIR, CUANDO NO HAGAMOS MODIFY, INSERT O DELETE. SÓLO LEER.
                    rlCustLedgEntry.SetAutoCalcFields(Amount); //CALCULA CAMPOS CALCULADOS DIRECTAMENTE
                    if rlCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlCustLedgEntry.Amount;
                        until rlCustLedgEntry.Next() = 0;
                    end;
                    Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }

            action(MMSumatorio04)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 04';
                trigger OnAction()
                var
                    rlCustLedgEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    rlCustLedgEntry.CalcSums(Amount); //NO SE PUEDE HACER SOBRE CAMPOS CALCULADOS
                    xlTotal := rlCustLedgEntry.Amount;
                    Message('Total = %1. Ha tardado %2', xlTotal, CurrentDateTime - xlInicio);
                end;
            }
            action(MMExportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar Excel';
                Image = Excel;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xFila: Integer;
                    xColumna: Integer;
                begin
                    TempLExcelBuffer.CreateNewBook('Clientes');
                    //Rellenar las celdas de excel
                    //CABECERA
                    CrearCabeceraF(TempLExcelBuffer);
                    //CABECERA

                    //FILAS
                    // if Page.RunModal(0) in [Action::LookupOK, Action::OK, Action::Yes] then begin ABRE UNA pÁGINA ENTONCES NO HACE FALTA EJECUTAR EL PAGE.RUNMODAL
                    CurrPage.SetSelectionFilter(rlCustomer);
                    //CALCULAR CAMPOS CALCULADOS
                    rlCustomer.SetAutoCalcFields("Balance (LCY)", Balance, "Sales (LCY)", "Payments (LCY)");
                    rlCustomer.SetLoadFields("No.", Name, "Responsibility Center", "Location Code", "Phone No.", Contact, "Balance (LCY)", Balance, "Sales (LCY)", "Payments (LCY)");
                    if rlCustomer.FindSet(false) then begin
                        xFila := 2;
                        repeat
                            AsignaCeldaF(xFila, 1, rlCustomer."No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 2, rlCustomer.Name, false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 3, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 4, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 5, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 6, rlCustomer.Contact, false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 7, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 8, Format(rlCustomer.Balance), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 9, Format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(xFila, 10, Format(rlCustomer."Payments (LCY)"), false, false, TempLExcelBuffer);
                            xFila += 1;
                        until rlCustomer.Next() = 0;
                    end else begin
                        Error('Nada seleccionado');
                    end;



                    //FILAS

                    TempLExcelBuffer.WriteSheet('', '', '');
                    TempLExcelBuffer.CloseBook();
                    TempLExcelBuffer.OpenExcel();
                end;
            }
            action(MMImportarExcel01)
            {
                ApplicationArea = All;
                Caption = 'Importar Excel';
                Image = Import;
                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    xlFichero: Text;
                    xlInStr: InStream;
                    xlHoja: Text;
                    rlCustomer: Record Customer;
                    i: Integer;
                begin
                    // if UploadIntoStream('Seleccione el fichero excel para importar clientes', '', 'Ficheros Excel (*.xls)|*.xls;*xlsx', xlFichero, xlInStr) then begin
                    if UploadIntoStream('', xlInStr) then begin
                        xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStr);
                        if xlHoja = '' then begin
                            Error('Proceso cancelado');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInStr, xlHoja);
                        TempLExcelBuffer.ReadSheet();
                        TempLExcelBuffer.SetFilter("Row No.", '<>%1', 1);
                        if TempLExcelBuffer.FindSet(false) then begin
                            repeat
                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                    3:
                                        rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                    4:
                                        rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                    5:
                                        rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                    6:
                                        rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                    7:
                                        rlCustomer.Modify(true);

                                end;





                            until TempLExcelBuffer.Next() = 0;

                        end;
                    end else begin
                        Error('No se ha podido cargar el fichero');
                    end;

                end;
            }

            action(MMPaginaDeFiltros)
            {
                ApplicationArea = All;
                Caption = 'Página de filtros';
                trigger OnAction()
                var
                    xlPaginaFiltros: FilterPageBuilder;
                    rlCustomer: Record Customer;
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    rlSalesPrice: Record "Sales Price";
                    rlVendor: Record Vendor;
                    clClientes: Label 'Clientes';
                    clMovimientos: Label 'Movimientos';
                    clPrecios: Label 'Precios';
                    pglMMConfiguracionC01: Page MMConfiguracionC01;
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los clientes a procesar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFiltros.AddField(clClientes, rlCustomer.MMFechaUltVacunaC01);
                    xlPaginaFiltros.AddRecord(clMovimientos, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clMovimientos, rlCustLedgerEntry."Bill No.");
                    xlPaginaFiltros.AddField(clMovimientos, rlCustLedgerEntry."Amount (LCY)");
                    xlPaginaFiltros.AddRecord(clPrecios, rlSalesPrice);
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Item No.");
                    xlPaginaFiltros.AddField(clPrecios, rlSalesPrice."Price Includes VAT");
                    if xlPaginaFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPaginaFiltros.GetView(clClientes, true)); //PODEMOS APLICAR GETVIEW (FILTROS) A OTRA TABLA DISTINTA Y FILTRARÁ POR LOS ATRIBUTOS POSIBLES
                        rlVendor.SetView(xlPaginaFiltros.GetView(clClientes, true)); // SEGUNDO PARAMETRO INDICA QUE SE FILTRA POR NOMBRE
                        if rlCustomer.FindSet(false) then begin
                            repeat
                            //Hacer lo que sea
                            until rlCustomer.Next() = 0;
                        end;
                        Message(xlPaginaFiltros.GetView(clMovimientos, false)); //False para indicar que no filtre por nombre e intente hacer el filtro por Columna. Es decir, Columna 1 de una tabla con Columna 1 de la otra tabla.
                        Message(xlPaginaFiltros.GetView(clPrecios, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;

                end;
            }




        }
        addlast(History)
        {
            action(MMFacturasAbonosMostrarC01) //RELACIONADO HISTORIAL
            {
                ApplicationArea = All;
                Caption = 'Mostrar Facturas y Abonos';

                trigger OnAction()
                begin
                    FrasYAbonosF();
                end;

            }
        }
    }
    var
        xSwitch: Boolean;
        xcuMMFuncionesAppC01: Codeunit MMVblesGlobalesAppC01;

    local procedure FrasYAbonosF()
    var
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header"; //Hist fact vent
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header"; //hist abon vent
        plCustomerList: Page "Customer List";

        xNum: Code[20];
    begin
        //if rlSalesInvoiceHeader.ReadPermission 
        //if rlSalesInvoiceHeader.WritePermission
        rlSalesInvoiceHeader.SetRange("Sell-to Customer No.", Rec."No.");
        rlSalesCrMemoHeader.SetRange("Sell-to Customer No.", Rec."No.");
        //rlSalesCrMemoHeader.SetFilter("Bill-to Customer No.", rlSalesInvoiceHeader.GetFilter("Bill-to Customer No."))
        //Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No.")); ME DA EL CÓDIGO DE CLIENTE QUE FILTRA


        //rlSalesInvoiceHeader.GetView(); DA UNA INSTRUCCIÓN SQL



        //Recorremos las facturas del cliente y las insertamos en la tabla temporal
        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                xNum := InsStr(rlSalesInvoiceHeader."No.", 'F', 1);
                TempLSalesInvoiceHeader.Copy(rlSalesInvoiceHeader);
                TempLSalesInvoiceHeader."No." := xNum;
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesInvoiceHeader.Next() = 0;
        end;

        //rlSalesCrMemoHeader.SetView(rlRalesInvoiceHeader.GetView()); MUY IMPORTANTE PARA APLICAR FILTROS ENTRE TABLAS QUE SE RELACIONAN PERO NO TIENEN LOS MISMOS ATRIBUTOS

        //Message(rlSalesCrMemoHeader.GetFilters); 
        //Recorremos los abonos del  y los insertamos en la tabla temporal
        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                xNum := InsStr(rlSalesCrMemoHeader."No.", 'A', 1);
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader."No." := xNum;
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;


        //Mostramos la página con facturas y abonos
        //plCustomerList.Run(TempLSalesInvoiceHeader);
        Page.Run(0, TempLSalesInvoiceHeader);

    end;

    local procedure ExportarClienteFicheroF()
    var
        rlMMCabPlanVacunacionC01: Record MMCabPlanVacunacionC01;
        pglMMListaCabPlanVacunacionC01: Page MMListaCabPlanVacunacionC01;
        xlOutStr: OutStream;
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        culTypeHelper: Codeunit "Type Helper";
        xlSeparador: Label '·', Locked = true;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4';
        rlCustomer: Record Customer;
    begin
        TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        //SIRVE PARA SABER EN QUÉ REGISTRO ME ENCUENTRO SELECCIONADO
        rlCustomer := Rec; //SIRVE PARA SABER EN QUÉ REGISTRO ME ENCUENTRO SELECCIONADO
        rlCustomer.Find(); //SIRVE PARA SABER EN QUÉ REGISTRO ME ENCUENTRO SELECCIONADO
                           //SIRVE PARA SABER EN QUÉ REGISTRO ME ENCUENTRO SELECCIONADO

        rlCustomer.CalcFields("Balance (LCY)"); //PARA CAMPOS CALCULADOS
        //rlCustomer.CalcSums(); //FORMULA DE SUMAS, DENTRO DEL FILTRO QUE TIENES SOBRE UN CAMPO QUE SEA SUMABLE ME CALCULA LA SUMA
        //HAY QUE USAR CALCFIELDS PARA LOS CAMPOS BLOB

        xlOutStr.WriteText(StrSubstNo(clCadenaCabecera, xlSeparador, Rec."No.", Rec.Name, Rec."Balance (LCY)"));
        xlOutStr.WriteText();
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Problema al descargar fichero');
        end;

    end;

    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true); //
        TempPExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalica);
        TempPExcelBuffer.Modify(true);
    end;

    local procedure CrearCabeceraF(var TempLExcelBuffer: Record "Excel Buffer" temporary)
    begin
        AsignaCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 4, Rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 7, Rec.FieldCaption(Rec."Balance (LCY)"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 8, Rec.FieldCaption(Rec.Balance), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 9, Rec.FieldCaption(Rec."Sales (LCY)"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 10, Rec.FieldCaption(Rec."Payments (LCY)"), true, true, TempLExcelBuffer);
    end;



}
