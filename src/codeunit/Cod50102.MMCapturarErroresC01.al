codeunit 50102 "MMCapturarErroresC01"
{
    trigger OnRun()
    var
        rlCustomer: Record Customer;
        clCodCte: Label 'ABC', Locked = true;
    begin

        if not rlCustomer.Get(clCodCte) then begin
            rlCustomer.Init();
            rlCustomer.Validate("No.", clCodCte);
            rlCustomer.Insert(true);
            rlCustomer.Validate(Name, 'Cliente prueba');
            rlCustomer.Validate(Address, 'Mi calle');
            rlCustomer.Validate("Payment Method Code", 'JJ');
            rlCustomer.Modify(true);
        end;
        Message('Datos del cliente: %1', rlCustomer);
    end;

    procedure FuncionParaCapturarErrorF(pCodProveedor: Code[20]): Boolean
    var
        rlVendor: Record Vendor;
    begin
        if not rlVendor.Get(pCodProveedor) then begin
            rlVendor.Init();
            rlVendor.Validate("No.", pCodProveedor);
            rlVendor.Insert(true);
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true);
            end else begin
                Message('Error en asignar datos (%1)', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('Ya existe el proveedor %1', pCodProveedor);

    end;

    [TryFunction]
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor)
    begin
        rlVendor.Validate(Name, 'Proveedor de prueba');
        rlVendor."VAT Registration No." := 'xxxx'; //Para que no verifique si el cif está en otro proveedor
        rlVendor.Validate(Address, 'Mi Calle');
        rlVendor.Validate("Payment Method Code", 'JJ');
    end;

    [TryFunction]
    procedure CapturaValidacionesF(var pCampo: Record "Sales Header"; ptipo: Integer; pLinea: Text; pSeparador: Text)
    var
        xlTipo: Enum "Sales Document Type";
        xlFecha: Date;
        xlStatus: Enum "Sales Document Status";
    begin
        case ptipo of
            2:
                begin
                    pCampo.Validate("No.", pLinea.Split(pSeparador).Get(ptipo));
                end;
            3:
                begin
                    if Evaluate(xlTipo, pLinea.Split(pSeparador).Get(ptipo)) then begin
                        pCampo.Validate("Document Type", xlTipo);
                    end;
                end;
            4:
                begin
                    pCampo.Validate("Bill-to Name", pLinea.Split(pSeparador).Get(ptipo));
                end;
            5:
                begin
                    pCampo.Validate("Bill-to Contact", pLinea.Split(pSeparador).Get(ptipo));
                end;
            6:
                begin
                    pCampo.Validate("Bill-to Customer No.", pLinea.Split(pSeparador).Get(ptipo));
                end;
            7:
                begin
                    pCampo.Validate("Sell-to Customer No.", pLinea.Split(pSeparador).Get(ptipo));
                end;
            8:
                begin
                    if Evaluate(xlStatus, pLinea.Split(pSeparador).Get(ptipo)) then begin
                        pCampo.Validate("Status", xlStatus);
                    end;
                end;
            9:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparador).Get(ptipo)) then begin
                        pCampo.Validate("Posting Date", xlFecha);
                    end;
                end;
            10:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparador).Get(ptipo)) then begin
                        pCampo.Validate("Due Date", xlFecha);
                    end;
                end;
            11:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparador).Get(ptipo)) then begin
                        pCampo.Validate("Shipment Date", xlFecha);
                    end;
                end;

        end;


    end;

    [TryFunction]

    procedure CapturaValidacionesF(var pCampo: Record "Sales Line"; ptipo: Integer; pLinea: Text; pSeparador: Text)
    var

        xlLNum: Integer;
        xlTipo: Enum "Sales Document Type";
        xlAmount: Decimal;
        xltipoL: Enum "Sales Line Type";
    begin
        case ptipo of
            2:
                if Evaluate(xlLNum, pLinea.Split(pSeparador).Get(ptipo)) then begin
                    pCampo.Validate("Line No.", xlLNum);
                end;

            3:
                pCampo.Validate("Document No.", pLinea.Split(pSeparador).Get(3));
            4:
                if Evaluate(xlTipo, pLinea.Split(pSeparador).Get(ptipo)) then begin
                    pCampo.Validate("Document Type", xlTipo);
                end;
            5:
                if Evaluate(xltipoL, pLinea.Split(pSeparador).Get(ptipo)) then begin
                    pCampo.Validate(Type, xltipoL);
                end;

            6:
                pCampo.Validate("No.", pLinea.Split(pSeparador).Get(ptipo));
            7:
                pCampo.Validate(Description, pLinea.Split(pSeparador).Get(ptipo));
            8:
                if Evaluate(xlAmount, pLinea.Split(pSeparador).Get(ptipo)) then begin
                    pCampo.Validate(Amount, xlAmount);
                end;


        end;

    end;
}