codeunit 50103 "MMSuscripcionesC01"
{
    SingleInstance = true;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.");
    end;

    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'Link ID', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventLinkIDF(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        culMMFuncionesAPPC01: Codeunit MMFuncionesAPPC01;
    begin
        culMMFuncionesAPPC01.MensajeFraCompraF(PurchInvHdrNo);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDoc(var SalesHeader: Record "Sales Header")
    var
        culMMFuncionesAPPC01: Codeunit MMFuncionesAPPC01;
    begin
        culMMFuncionesAPPC01.CodeunitSalesPostOnAfterPostSalesDoc(SalesHeader);
    end;
}