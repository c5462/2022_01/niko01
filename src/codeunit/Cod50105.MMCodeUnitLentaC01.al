codeunit 50105 "MMCodeUnitLentaC01"
{
    trigger OnRun()
    begin
        EjecutarTareaF();
    end;

    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Desea ejecutar?');
        end;
        if xlEjecutar then begin
            InsertarLogF('Tarea Iniciada');
            Commit(); //No usar casi nunca
            Sleep(10000);
            InsertarLogF('Tarea finalizada');
        end;
    end;

    local procedure InsertarLogF(pMensaje: Text)
    var
        rlMMLogC01: Record MMLogC01;
    begin
        rlMMLogC01.Init();
        rlMMLogC01.Insert(true);
        rlMMLogC01.Validate(Mensaje, pMensaje);
        rlMMLogC01.Modify(true);
    end;
}