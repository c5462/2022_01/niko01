codeunit 50101 "MMVblesGlobalesAppC01"
{
    SingleInstance = true;
    procedure SwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    procedure SwitchF(): Boolean
    begin
        exit(xSwitch);
    end;

    procedure NombreClienteF(pName: Text[100])
    begin
        xNombreCliente := pName;
    end;

    procedure NombreClienteF(): Text
    begin
        exit(xNombreCliente);
    end;

    var
        xSwitch: Boolean;
        xNombreCliente: Text;
}