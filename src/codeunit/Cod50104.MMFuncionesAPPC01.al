codeunit 50104 "MMFuncionesAPPC01"
{

    procedure MensajeFraCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin

            Message('%1 ha creado la factura %2', UserId, PurchInvHdrNo);

        end;
    end;

    procedure EjemploVariablesES02F(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon Servicios');
    end;

    procedure EjemploArrayF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
        xlnumElementosArray: Integer;
        xlNumRandom: Integer;
    begin
        Randomize();
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := Format(i); // Del 1 al 20
        end;
        //xlnumElementosArray := CompressArray(mlNumeros); Comprimir 
        xlnumElementosArray := ArrayLen(mlNumeros);
        while xlnumElementosArray <> 0 do begin
            xlNumRandom := Random(xlnumElementosArray);
            Message('Número de bola %1', mlNumeros[xlNumRandom]);
            mlNumeros[xlNumRandom] := '';
            xlnumElementosArray := CompressArray(mlNumeros);
        end;
    end;

    procedure UltimoNumLinea(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;

    end;

    procedure UltimoNumLinea(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No."); //Ordena los registros por las claves seleccionadas
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure UltimoNumLinea(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No."); //Ordena los registros por las claves seleccionadas
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end;
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemploSobrecargaF(p1, p2, p3, false);
    end;

    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; pBooleano: Boolean)
    var
        xlNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 12.3;
        p3 := 'Niko';
        //Nuevo funcionalidad
        if pBooleano then begin
            xlNuevoNumero := p1 * p2;
        end;
    end;

    procedure usoFuncionEjemploF()
    begin
        EjemploSobrecargaF(10, 34.56, 'Hola Mundo');
    end;

    procedure SintaxisF()
    var
        xVentana: Dialog;
    begin
        /*
        Asignacion := 1;
        Concatenacion de Texto. xlTexto := 'Niko'; xlTexto += 'Ucra'; xlTexto := strsubstno ('%1 %2 %3','Niko','Ucra','Face');
        Función Round: Round (x+z,PRECISION,DIRECCION) PRECISION=1 número entero; 0.1 redondeo a 1 decimal; 0.5 redondeo a múltiplos de 5; 2 a pares; 3 a impares; '<' redondeo más bajo, '>' al más cercano, '=' método clásico de redondear.
        DecimalPlaces para visulizar campos.
        i := 9 div 2; Saca la parte entera
        i := 9 mod 2; Saca el resto
        Abs() valor absoluto
        not, and, or, xor(uno o el otro, pero no los dos)
        xBool := 'a' in ['a'..'z']
        //FUNCIONES MUY USADAS
        Message('Pablo espabila');
        Error('Proceso cancelado por el usuario')
        Error('Proceso cancelado por el usuario %1', UserId) MANDA EL MENSAJE PERO HACE UN ROLLBACK
        Commit() PARA GUARDAR EN BASE DE DATOS
        Error('')
        Confirm("STRING",VALOR POR DEFECTO). STRING= MENSAJE; VALOR POR DEFECTO SERÁ EL QUE ESTÉ POR DEFECTO
        i := StrMenu('ENVIAR','FACTURAR','ENVIAR Y FACTURAR',NºDefecto); ES UN MENÚ CON OPCIONES PARA SELECCIONAR. EL NºDEFECTO EMPIEZA EN 1;
        case i of
            0: Error('Proceso cancelado por el usuario');
            1: //Enviar pedido
            2: //Facturar pedido
            3: //Enviar y Facturar pedido
        Ventana.
        if GuiAllowed then begin
        xVentana.Open('PROCESANDO BUCLE #1#######\' + 'Procesando bucle x #2########');
        for i := 1 to 1000 do begin
            xVentana.Update(1, i); //NUMERO DE CONTROL Y LO QUE QUEREMOS MOSTRAR
            for x := 1 to 1000 do begin
                xVentana.Update(2, x); //NUMERO DE CONTROL Y LO QUE QUEREMOS MOSTRAR

            end;
        end;
        
        SI HAY MENSAJES O QUE SE MUESTRA ALGO AL USUARIO HAY QUE USAR GuiAllowed!
        xVentana.Close();
        end;
        */

        // Funciones de cadenas de texto
        /*
        xlLong := MaxStrLen(); Devuelve la longitud máxima que puede tener una cadena
      xlTexto :=  CopyStr('ABC',2,1) Copia de una cadena a otra cadena AB
      xlTexto := CopyStr('ABC',2) //BC
      xlTexto := xlTexto.Substring(2) Desde donde quiero empezar hasta donde
      xlPos := StrPos('Elefante','e') Devuelve la posición de un String dentro de un Substring. En una cadena donde empieza la subcadena. Devolvería un 3.
      xlLong := StrLen('ABCD      ') Longitud del string. Devuelve un 4
      UpperCase(xlTexto)
      LowerCase(xlTexto)
      xlTexto.ToUpper();
      xlTexto.ToLower();
      xlTexto := xlTexto.TrimEnd('\') + '\';
      xlTexto := xlTexto.TrimStrat('\').TrimEnd('\').Trim() + '\';
      xlTexto := '123,456,Tecon Servicios, s.l.,789'; Coger alguno de estos utilizar 
      Message(SelectStr(4,xlTexto)); s.l.
      xlTexto.Split('·').Get(4) Devuelve Lista de Textos 789
      xlCampos := xlTexto.Split('·');   xlCampos List de tipo Texto
      message(xlTexto.Split(clSeparador).Get(1))
      message(xlTexto.Split(clSeparador).Get(2))
      message(xlTexto.Split(clSeparador).Get(3))
      message(xlTexto.Split(clSeparador).Get(4))
      foreach x in j do beginend;
       Locked para que no traduzca los carácteres 
    Evaluate(xlFecha,ConvertStr('25-05-2022','-','/'); PARA CONVERTIR STRING A Cualquier dato
    FORMAT() DATO A STRING
    ConvertStr y Replace hace lo mismo. xlTexto.Replace('.','-')
    Message(DelChr(xlTexto,'<','e')) Principio
    Message(DelChr(xlTexto,'>','e')) Final
    Message(DelChr(xlTexto,'=','e')) En medio
    Message(DelChr(xlTexto,'<=>','e')) Principio, medio y final
    Power(3,2);
    UserId(); Usuario
    CompanyName(); En qué empresa me he logueado
    Today; Día en el que estoy
    WorkDate();
    Time; Hora local


    // MANEJO DE FICHEROS
    Uso de variables Stream.
    Out.Write
    In.Read
            */
        /*SetCurrentKey
        SetRange
        SetFilter
        Get().

        //FUNCIONES FIND
        FIND es la más lenta, para buscar registros internamente. Sólo en un caso, si que respeta filtros, se utiliza cuando 
        rlCustomer.SetFilter(Name,'%1 | %2', '*A*','*B*'); @ para distinguir entre Aa
        if rlCustomer.FindSet(true,true) then begin //SI FindSet() es false
            repeat 
            rlCustomer2 := rlCustomer;
            rlCustomer2.Find(); ES COMO HACER UN GET PERO SIN TENER QUE PASAR GET
            rlCustomer2.Validate(Name,DelChr(rlCustomer2.Name,'<=>','AB'));
            rlCustomer2.Modify(true);
            until rlCustomer.Next()=0; //PARA VER SI ESTO FUNCIONA ME HAGO un CUSTOMER2

        */

    end;

    //X caracteres de la izquierda o de la derecha
    procedure LeftF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        if not (pNumCaracteres > StrLen(pCadena)) then begin
            exit(CopyStr(pCadena, 1, pNumCaracteres));
        end else begin
            Error('Número de carácteres superior a la longitud de la cadena de entrada');
        end;
    end;

    procedure RightF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        if not (pNumCaracteres > StrLen(pCadena)) then begin
            exit(CopyStr(pCadena, StrLen(pCadena) - pNumCaracteres + 1))
        end else begin
            Error('Número de carácteres superior a la longitud de la cadena de entrada');
        end;
    end;

    procedure StreamsF()
    var
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        xlInStr: InStream;
        xlOutStr: OutStream;
        //LAS TEMPORALES NO TIENEN VALIDATE Y DELETE TRUE. ASIK DELETE FALSE SIEMPRE
        //BLOB EN TABLAS
        xlLinea: Text;
        xlNombreFichero: Text;
    begin
        TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
        xlOutStr.WriteText('MMM');
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        xlNombreFichero := 'Pedido.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Error 404');
        end;

    end;

    procedure GetClienteF()
    var
        rlCustomer: Record Customer;
    begin
        if not rlCustomer.Get('10000') then begin
            Error('No existe el cliente');
        end;
    end;

    procedure GetLineaRegistroF()
    var
        rlSalesLine: Record "Sales Line";
    begin
        rlSalesLine.Get(rlSalesLine."Document Type"::Order, '101005', 10000);
    end;

    procedure GetDimensionF()
    var
        rlDimensionSetEntry: Record "Dimension Set Entry";
    begin
        rlDimensionSetEntry.Get(7, 'GRUPONEGOCIO');
    end;

    procedure GetEmpresaF()
    var
        rlCompanyInformation: Record "Company Information";
    begin
        rlCompanyInformation.Get();
    end;

    local procedure otrasFuncionesF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    procedure UsoDeListaF(pView: Text)
    var
        xlLista: List of [Code[20]];
        rlCustLedgerEntry: Record "Cust. Ledger Entry";
        xlDic: Dictionary of [Code[20], Decimal];
        xlDic2: Dictionary of [Code[20], List of [Decimal]];
    begin
        rlCustLedgerEntry.SetView(pView);
        rlCustLedgerEntry.SetLoadFields("Sell-to Customer No.");
        if rlCustLedgerEntry.FindSet(false) then begin
            repeat
                if not xlLista.Contains(rlCustLedgerEntry."Sell-to Customer No.") then begin
                    xlLista.Add(rlCustLedgerEntry."Sell-to Customer No.");
                end
            until rlCustLedgerEntry.Next() = 0;
            Message('Nº de clientes en mov clientes es de %1', xlLista.Count);
        end else begin
            Message('No existen registros dentro del filtro %1', rlCustLedgerEntry.GetFilters);
        end;
    end;

    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        nombreDato: Label 'CódigoCliente';
    begin

        if pNotificacion.HasData(nombreDato) then begin
            if rlCustomer.Get(pNotificacion.GetData(nombreDato)) then begin
                Page.Run(Page::"Customer Card", rlCustomer);
            end;
        end;
    end;

    procedure CodeunitSalesPostOnAfterPostSalesDoc(var SalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        xlNotificacion: Notification;
        rlSalesLine: Record "Sales Line";
        MensajeLinea: Label 'La linea %1 no supera los 100 euros';
    begin
        if rlCustomer.Get(SalesHeader."Sell-to Customer No.") then begin
            if rlCustomer.MMTipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo('El cliente %1 no tiene ninguna vacuna', rlCustomer.Name));
                xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::MMFuncionesAPPC01, 'AbrirListaClientesF');
                xlNotificacion.SetData('CódigoCliente', rlCustomer."No.");
                xlNotificacion.Send();
            end;
        end;
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetLoadFields(Amount, "Line No.");
        if rlSalesLine.FindSet(false) then begin
            repeat
                if rlSalesLine.Amount < 100 then begin
                    Clear(xlNotificacion);
                    xlNotificacion.Id(CreateGuid());
                    xlNotificacion.Message(StrSubstNo(MensajeLinea, rlSalesLine."Line No."));
                    xlNotificacion.Send();
                end;
            until rlSalesLine.Next() = 0;
        end;
    end;

    procedure CompruebaCodenFiltroF(pCodigo: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodigo;
        TempLCustomer.Insert(false);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;
}
