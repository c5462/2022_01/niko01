tableextension 50100 "MMCustomerExtC01" extends Customer
{

    fields
    {
        field(50100; MMNumVacunasC01; Integer)
        {
            Editable = false;
            Caption = 'Nº de vacunas ';
            FieldClass = FlowField;
            CalcFormula = count(MMLinPlanVacunacionC01 where(Cliente = field("No.")));
        }
        field(50101; MMFechaUltVacunaC01; Date)
        {
            Caption = 'Fecha de última vacuna';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = max(MMLinPlanVacunacionC01.FechaVacunacion where(Cliente = field("No.")));
        }
        field(50102; MMTipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de vacuna';
            DataClassification = CustomerContent;
            TableRelation = MMVariosC01.Codigo where(Tipo = const(Vacuna));

            trigger OnValidate()
            var
                rlMMVariosC01: Record MMVariosC01;
            begin
                if rlMMVariosC01.Get(rlMMVariosC01.Tipo::Vacuna, Rec.MMTipoVacunaC01) then begin
                    rlMMVariosC01.TestField(Bloqueado, false);
                end;
            end;
        }
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culMMVblesGlobalesAppC01: Codeunit MMVblesGlobalesAppC01;
            begin
                culMMVblesGlobalesAppC01.NombreClienteF(Rec.Name);
            end;
        }


    }
    trigger OnAfterInsert()
    begin
        //FILTRAR POR EMPRESAS DISTINTAS A LA QUE ESTAMOS
        SincronizarRegistrosF(xAccion::Insert);
    end;

    trigger OnAfterModify()
    begin
        SincronizarRegistrosF(xAccion::Modify);
    end;


    trigger OnAfterDelete()
    begin
        SincronizarRegistrosF(xAccion::Delete);
    end;


    local procedure SincronizarRegistrosF(pAccion: Option)
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        rlCompany.SetFilter(Name, '<>%1', CompanyName);
        //RECORRER LAS EMPRESAS
        if rlCompany.FindSet(false) then begin
            repeat
                //CAMBIAR LA VARIABLE LOCAL DE REGISTRO DE CLIENTES
                rlCustomer.ChangeCompany(rlCompany.Name);
                //ASIGNAR LOS VALORES DE CLIENTE A LA VARIABLE LOCAL
                rlCustomer.Init();
                rlCustomer.Copy(Rec);
                case pAccion of
                    xAccion::Insert:
                        rlCustomer.Insert(false); //Sin TRUE Para que no cree los contactos, ya que los crea en la empresa en la que estamos 
                    xAccion::Modify:
                        rlCustomer.Modify(false);
                    xAccion::Delete:
                        rlCustomer.Delete(false);
                end
            //INSERTAR EL CLIENTE
            until rlCompany.Next() = 0;
        end;
    end;

    var
        xAccion: Option Insert,Modify,Delete;
}
