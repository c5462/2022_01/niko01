tableextension 50101 "MMVendorExtC01" extends Vendor
{
    trigger OnAfterInsert()
    var
        culMMVblesGlobalesAppC01: Codeunit MMVblesGlobalesAppC01;
    begin
        Rec.Validate(Name, culMMVblesGlobalesAppC01.NombreClienteF());
    end;
}
