page 50100 "MMConfiguracionC01"
{
    Caption = 'Configuración de la app 01 del curso';
    PageType = Card;
    SourceTable = MMConfiguracionC01;
    AboutTitle = 'En esta página se refleja la configuración de la primera app del curso de Especialistas de Business Central 2022';
    AboutText = 'Configure la app de forma correcta. Asegurese que asigna el valor del campo de cliente web';
    AdditionalSearchTerms = 'Curso 01';
    UsageCategory = Administration;
    ApplicationArea = All;
    DeleteAllowed = false;
    InsertAllowed = false;
    layout
    {
        area(content)
        {

            group(General)
            {
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Code. cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    Importance = Promoted;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo de texto para registro';
                    AboutText = 'Nos permitirá rellenar el campo';
                }
                field(Contraseña; Rec.IdPassword)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo de contraseña para registro';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo documento field.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(NombreCodTabla; Rec.NombreTablaF(Rec.TipoTabla, rec.CodTabla))
                {
                    Caption = 'Nombre tabla';
                    ApplicationArea = All;
                    Editable = false;
                }

                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the ColorFondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the ColorLetra field.';
                    ApplicationArea = All;
                }

                field(xNumInteracciones; xNumInteracciones)
                {
                    ApplicationArea = All;
                    Caption = 'Nº de interacciones';

                }



            }
            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }

            }
            group(CalculatedFields)
            {
                field(NomCliente; Rec.NombreCliente)
                {

                    ToolTip = 'Nombre del cliente web';
                    ApplicationArea = All;
                }
                field(NomClienteF; Rec.NombreClienteF(Rec.CodCteWeb))
                {
                    Caption = 'Nombre cliente';
                    ApplicationArea = All;
                }

            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'Mensaje de acción';
                Image = AddAction;
                trigger OnAction()
                begin
                    Message('Acción pulsada');
                end;
            }
            action(CambiarContraseña)
            {
                ApplicationArea = All;
                Caption = 'Cambiar Contraseña';
                trigger OnAction()
                begin
                    CambiarPasswordF();
                end;
            }
            action(ProbarFuncTexto)
            {
                ApplicationArea = All;
                Caption = 'Probar funciones de texto';
                trigger OnAction()
                var
                    culMMFuncionesAPPC01: Codeunit MMFuncionesAPPC01;
                begin
                    Message('Caracteres: %1', culMMFuncionesAPPC01.LeftF('Pepito', 3));
                    Message('Caracteres: %1', culMMFuncionesAPPC01.RightF('Pepito', 3));
                end;
            }
            action(ExportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Exportar Vacunas';
                trigger OnAction()
                var

                begin
                    ExportarVacunasF();
                end;
            }
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'Pedir Datos Varios';
                trigger OnAction()
                var
                    pglMMEntradaDeDatosVariosC01: Page MMEntradaDeDatosVariosC01;
                    xlBool: Boolean;
                    xlDecimal: Decimal;
                    xlFecha: Date;
                begin
                    pglMMEntradaDeDatosVariosC01.CampoF('Ciudad de nacimiento', 'Albacete');
                    pglMMEntradaDeDatosVariosC01.CampoF('País de nacimiento', '');
                    pglMMEntradaDeDatosVariosC01.CampoF('Comunidad de nacimiento', 'CLM');
                    pglMMEntradaDeDatosVariosC01.CampoF('4 campo', 'holi');
                    pglMMEntradaDeDatosVariosC01.CampoF('Cuota pagada', false);
                    pglMMEntradaDeDatosVariosC01.CampoF('Cuota 1er trimestre pagada', false);
                    pglMMEntradaDeDatosVariosC01.CampoF('Cuota 2 trimestre pagada', true);
                    pglMMEntradaDeDatosVariosC01.CampoF('Cuota 1 ', 0.0);
                    pglMMEntradaDeDatosVariosC01.CampoF('Cuota 2 ', 5.5);
                    pglMMEntradaDeDatosVariosC01.CampoF('Fecha Cuota Pagada 1 ', Today);
                    pglMMEntradaDeDatosVariosC01.CampoF('Fecha Cuota Pagada 2 ', Today);

                    if pglMMEntradaDeDatosVariosC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
                        Message(pglMMEntradaDeDatosVariosC01.CampoF());
                        Message(pglMMEntradaDeDatosVariosC01.CampoF());
                        Message(pglMMEntradaDeDatosVariosC01.CampoF());
                        Message(pglMMEntradaDeDatosVariosC01.CampoF());
                        Message('Cuota 1 %1', pglMMEntradaDeDatosVariosC01.CampoF(xlBool));
                        pglMMEntradaDeDatosVariosC01.CampoF(xlBool);
                        Message('Cuota 2 %1', xlBool);
                        Message('Cuota 3 %1', xlBool);
                        Message('Cuota 1 %1', pglMMEntradaDeDatosVariosC01.CampoF(xlDecimal));
                        Message('Cuota 2 %1', pglMMEntradaDeDatosVariosC01.CampoF(xlDecimal));
                        Message('Fecha cuota 1 %1', pglMMEntradaDeDatosVariosC01.CampoF(xlFecha));
                        Message('Fecha cuota 2 %1', pglMMEntradaDeDatosVariosC01.CampoF(xlFecha));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(SeleccionarClientes)
            {
                Caption = 'Seleccionar Clientes del Almacen Gris';
                ApplicationArea = All;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    xlFiltroGrupo: Integer;
                    rlCompany: Record Company;
                begin
                    // Mostrar la lista de empresas de la BBDD y de la seleccionada mostrar sus clientes GRISES
                    if AccionAceptadaF(page.RunModal(page::Companies, rlCompany)) then begin
                        rlCustomer.ChangeCompany(rlCompany.Name);
                        xlFiltroGrupo := rlCustomer.FilterGroup();
                        rlCustomer.FilterGroup(xlFiltroGrupo + 20); // CAMBIO EL FILTRO PARA EVITAR QUE EL USUARIO ELIJA ALGO QUE NO DEBA ELEGIR.
                        rlCustomer.SetRange("Location Code", 'GRIS');
                        rlCustomer.FilterGroup(xlFiltroGrupo);
                        if AccionAceptadaF(page.RunModal(page::"Customer List", rlCustomer)) then begin
                            Message('Proceso OK');
                        end else begin
                            Error('Proceso cancelado por el usuario');
                        end;
                    end
                    //RunModal para el cliente pueda aceptar o cancelar la acción
                    //SetRecFilter APLICA EL FILTRO DEL REGISTRO QUE TENGO SELECCIONADO, APLICANDO EL FILTRO DE ESE REGISTRO. ¡¡¡¡¡¡¡DENTRO DE LA MISMA TABLA!!!!!!!!!!
                end;
            }
            action(ImportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Importar Vacunas';
                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }

            action(PruebaTxt)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text';

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: Text;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumInteracciones do begin
                        xlTxt += '.';

                    end;
                    Message('Tiempo invertido %1', CurrentDateTime - xlInicio);
                end;
            }
            action(PruebaTxtBuilder)
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text Builder';

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTxt: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to xNumInteracciones do begin
                        xlTxt.Append('.');
                    end;
                    Message('Tiempo invertido %1 %2', CurrentDateTime - xlInicio, xlTxt.ToText());
                end;
            }
            action(MMCuentaClientesC01)
            {
                ApplicationArea = All;
                Caption = 'Cuenta Clientes';
                trigger OnAction()
                var
                    culMMFuncionesAPPC01: Codeunit MMFuncionesAPPC01;
                    xlPaginaFiltros: FilterPageBuilder;
                    clClientes: Label 'Movimientos de clientes';
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                begin
                    xlPaginaFiltros.PageCaption('Seleccione los registros a contar');
                    xlPaginaFiltros.AddRecord(clClientes, rlCustLedgerEntry);
                    xlPaginaFiltros.AddField(clClientes, rlCustLedgerEntry."Posting Date");
                    if xlPaginaFiltros.RunModal() then begin
                        culMMFuncionesAPPC01.UsoDeListaF(xlPaginaFiltros.GetView(clClientes, true));
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }


        }
    }
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;

    [NonDebuggable]
    local procedure CambiarPasswordF()
    var
        pglMMEntradaDeDatosVariosC01: Page MMEntradaDeDatosVariosC01;
        xlPassN: Text;
        xlPassV: Text;
    begin
        pglMMEntradaDeDatosVariosC01.CampoF('Contraseña actual', '', true);
        pglMMEntradaDeDatosVariosC01.CampoF('Contraseña nueva', '', true);
        pglMMEntradaDeDatosVariosC01.CampoF('Confirmar contraseña', '', true);
        if pglMMEntradaDeDatosVariosC01.RunModal() in [Action::OK, Action::Yes, Action::LookupOK] then begin
            pglMMEntradaDeDatosVariosC01.CampoF(xlPassN);
            if Rec.PasswordF() = xlPassN then begin

                if pglMMEntradaDeDatosVariosC01.CampoF(xlPassN) = pglMMEntradaDeDatosVariosC01.CampoF(xlPassV) then begin
                    Rec.PasswordF(xlPassV);
                    Message('Contraseña cambiada');
                end else begin
                    Error('Contraseñas no coinciden')
                end;

            end else begin
                Error('Contraseñas actuales no coinciden');
            end;
        end else begin
            Error('Proceso cancelado por el usuario')
        end;
    end;

    local procedure ExportarVacunasF()
    var
        rlMMCabPlanVacunacionC01: Record MMCabPlanVacunacionC01;
        pglMMListaCabPlanVacunacionC01: Page MMListaCabPlanVacunacionC01;
        xlOutStr: OutStream;
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
        xlNombreFichero: Text;
        xlInStr: Instream;
        xlLinea: Text;
        culTypeHelper: Codeunit "Type Helper";
        xlSeparador: Label '·', Locked = true;
    begin
        Message('Seleccione vacunas a exportar');
        pglMMListaCabPlanVacunacionC01.LookupMode(true);
        pglMMListaCabPlanVacunacionC01.SetRecord(rlMMCabPlanVacunacionC01);

        if pglMMListaCabPlanVacunacionC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            //Obtenemos el dataset
            pglMMListaCabPlanVacunacionC01.GetSelectionFilterF(rlMMCabPlanVacunacionC01); //PARA OBTENER VARIOS CAMPOS SELECCIONADOS
            if rlMMCabPlanVacunacionC01.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                TempLMMConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::UTF8);
                repeat

                    xlOutStr.WriteText('C');
                    xlOutStr.WriteText(xlSeparador);
                    xlOutStr.WriteText(rlMMCabPlanVacunacionC01.NombreEmpresaVacunadoraF());
                    xlOutStr.WriteText(xlSeparador);
                    xlOutStr.WriteText(rlMMCabPlanVacunacionC01.CodigoCabecera);
                    xlOutStr.WriteText(xlSeparador);
                    xlOutStr.WriteText(rlMMCabPlanVacunacionC01.Descripcion);
                    xlOutStr.WriteText(xlSeparador);
                    xlOutStr.WriteText(rlMMCabPlanVacunacionC01.EmpresaVacunadora);
                    xlOutStr.WriteText(xlSeparador);
                    xlOutStr.WriteText(culTypeHelper.CRLFSeparator());
                    //RECORREMOS LAS LÍNEAS
                    if rlMMLinPlanVacunacionC01.FindSet(false) then begin //SI MODIFICO (O BIEN LA CALVE PRINCIPAL O UN CAMPO QUE PERTENEZCA A ESE FILTRO HAY QUE PONER EL SEGUNDO TRUE): TRUE, TRUE
                        rlMMLinPlanVacunacionC01.SetRange(CodigoLineas, rlMMCabPlanVacunacionC01.CodigoCabecera); //1 p: campo por el que queremos filtrar el parametro. 2p: "DESDE" | VALOR, si no pongo el 3, estoy diciendo que filtrame el campo 1 con este VALOR. Si le ponemos el 3p: "HASTA".
                        //SETFILTER. 3 valores. Primero: Campo. Segundo: Cadena. Tecero: Valores.
                        repeat
                            xlOutStr.WriteText('L');
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(rlMMLinPlanVacunacionC01.CodigoLineas);
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(rlMMLinPlanVacunacionC01.Cliente);
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(rlMMLinPlanVacunacionC01.CodigoOtros);
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(rlMMLinPlanVacunacionC01.CodigoVacuna);
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01.FechaVacunacion));
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01.N_Linea));
                            xlOutStr.WriteText(xlSeparador);
                            xlOutStr.WriteText(Format(rlMMLinPlanVacunacionC01.SiguienteFechaVacuna));
                            xlOutStr.WriteText(culTypeHelper.CRLFSeparator())
                        until rlMMLinPlanVacunacionC01.Next() = 0;
                    end;
                until rlMMCabPlanVacunacionC01.Next() = 0;

            end;

        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        xlNombreFichero := 'Pedido.csv';
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlLinea);
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('Error 404');
        end;
    end;

    local procedure ImportarVacunasF()
    var
        xlInStr: Instream;
        xlLinea: Text;
        rlMMCabPlanVacunacionC01: Record MMCabPlanVacunacionC01;
        rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
        TempLMMConfiguracionC01: Record MMConfiguracionC01 temporary;
        clSeparador: Label '·', Locked = true;
        xlFecha: Date;
        xlLNum: Integer;
    begin
        TempLMMConfiguracionC01.MiBlob.CreateInStream(xlInStr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInStr) then begin
            while not xlInStr.EOS do begin //EOS END OF STRING. LEE HASTA EL FINAL.
                xlInStr.ReadText(xlLinea);
                //Insertar Cabecera o líneas en funcion del primer caracter de la línea
                case true of
                    xlLinea[1] = 'C':
                        //DAR DE ALTA CABECERA
                        begin
                            rlMMCabPlanVacunacionC01.Init();
                            rlMMCabPlanVacunacionC01.Validate(CodigoCabecera, xlLinea.Split(clSeparador).Get(2));
                            rlMMCabPlanVacunacionC01.Insert(true);
                            rlMMCabPlanVacunacionC01.Validate(Descripcion, xlLinea.Split(clSeparador).Get(3));
                            rlMMCabPlanVacunacionC01.Validate(EmpresaVacunadora, xlLinea.Split(clSeparador).Get(4));

                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(5)) then begin
                                rlMMCabPlanVacunacionC01.Validate(FechaVacunacionPlanificada, xlFecha);
                            end;
                            rlMMCabPlanVacunacionC01.Modify(true);
                        end;
                    xlLinea[1] = 'L':
                        //DAR DE ALTA LÍNEA
                        begin
                            //"Document Type", "Document No.", "Line No."
                            rlMMLinPlanVacunacionC01.Init();
                            rlMMLinPlanVacunacionC01.Validate(CodigoLineas, xlLinea.Split(clSeparador).Get(2));
                            if Evaluate(xlLNum, xlLinea.Split(clSeparador).Get(7)) then begin
                                rlMMLinPlanVacunacionC01.Validate(N_Linea, xlLNum);
                            end;
                            rlMMLinPlanVacunacionC01.Insert(true);
                            rlMMLinPlanVacunacionC01.Validate(Cliente, xlLinea.Split(clSeparador).Get(3));
                            rlMMLinPlanVacunacionC01.Validate(CodigoOtros, xlLinea.Split(clSeparador).Get(4));
                            rlMMLinPlanVacunacionC01.Validate(CodigoVacuna, xlLinea.Split(clSeparador).Get(5));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(6)) then begin
                                rlMMLinPlanVacunacionC01.Validate(FechaVacunacion, xlFecha);
                            end;
                            if Evaluate(xlFecha, xlLinea.Split(clSeparador).Get(8)) then begin
                                rlMMLinPlanVacunacionC01.Validate(SiguienteFechaVacuna, xlFecha);
                            end;
                            rlMMLinPlanVacunacionC01.Modify(true);

                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Error (%1', GetLastErrorText());
        end;
    end;



    procedure AccionAceptadaF(pAccion: Action): Boolean
    begin
        exit(pAccion in [Action::LookupOK, Action::OK, Action::Yes]);
    end;

    var
        xNumInteracciones: Integer;

}
