page 50102 "MMFichaVariosC01"
{
    Caption = 'FichaVariosC01';
    PageType = Card;
    SourceTable = MMVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar registros de una tabla donde tendremos tipos varios de registros';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(PeriodoSegundaVacunacion; Rec.PeriodoSegundaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Periodo Segunda Vacunación field.';
                    ApplicationArea = All;
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
