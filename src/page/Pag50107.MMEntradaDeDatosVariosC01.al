page 50107 "MMEntradaDeDatosVariosC01"
{
    Caption = 'Entrada de datos varios';
    PageType = StandardDialog;
    layout
    {
        area(content)
        {
            field(Texto01; xTextos[1])
            {
                ApplicationArea = All;
                Visible = xVisibleTexto01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];

            }
            field(Texto02; xTextos[2])
            {
                ApplicationArea = All;
                Visible = xVisibleTexto02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }
            field(Texto03; xTextos[3])
            {
                ApplicationArea = All;
                Visible = xVisibleTexto03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; xTextos[4])
            {
                ApplicationArea = All;
                Visible = xVisibleTexto04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }
            field(Bool01; mBools[1])
            {
                ApplicationArea = All;
                Visible = xVisibleBool01;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; mBools[2])
            {
                ApplicationArea = All;
                Visible = xVisibleBool02;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; mBools[3])
            {
                ApplicationArea = All;
                Visible = xVisibleBool03;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }
            field(Dec01; mDecs[1])
            {
                ApplicationArea = All;
                Visible = xVisibleDec01;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 1];
            }
            field(Dec02; mDecs[2])
            {
                ApplicationArea = All;
                Visible = xVisibleDec02;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 2];
            }
            field(Dec03; mDecs[3])
            {
                ApplicationArea = All;
                Visible = xVisibleDec03;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 3];
            }
            field(Date01; mDates[1])
            {
                ApplicationArea = All;
                Visible = xVisibleDate01;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
            }
            field(Date02; mDates[2])
            {
                ApplicationArea = All;
                Visible = xVisibleDate02;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
            }
            field(Date03; mDates[3])
            {
                ApplicationArea = All;
                Visible = xVisibleDate03;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
            }
            field(Pass01; mPass[1])
            {
                ApplicationArea = All;
                Visible = xVisiblePass01;
                CaptionClass = mCaptionClass[xTipoDato::Password, 1];
                ExtendedDatatype = Masked;
            }
            field(Pass02; mPass[2])
            {
                ApplicationArea = All;
                Visible = xVisiblePass02;
                CaptionClass = mCaptionClass[xTipoDato::Password, 2];
                ExtendedDatatype = Masked;
            }
            field(Pass03; mPass[3])
            {
                ApplicationArea = All;
                Visible = xVisiblePass03;
                CaptionClass = mCaptionClass[xTipoDato::Password, 3];
                ExtendedDatatype = Masked;
            }

        }
    }
    var
        xTextos: array[10] of Text;
        mBools: array[10] of Boolean;
        xVisibleTexto01: Boolean;
        xVisibleTexto02: Boolean;
        mCaptionClass: array[8, 10] of Text;
        mPunteros: array[2, 8] of Integer;
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Fecha,Hora,Password;
        xTipoPuntero: Option Nulo,Pasado,Leido;


        xVisibleTexto03: Boolean;
        xVisibleBool01: Boolean;
        xVisibleBool02: Boolean;
        xVisibleBool03: Boolean;
        mDecs: Array[10] of Decimal;
        xVisibleDec01: Boolean;
        xVisibleDec02: Boolean;
        xVisibleDec03: Boolean;
        mDates: Array[10] of Date;
        xVisibleDate01: Boolean;
        xVisibleDate02: Boolean;
        xVisibleDate03: Boolean;
        mPass: Array[10] of Text;
        xVisiblePass01: Boolean;
        xVisiblePass02: Boolean;
        xVisiblePass03: Boolean;
        xVisibleTexto04: Boolean;


    procedure CampoF(pCaption: Text; pValorInicial: Text)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xTipoDato::Texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        xTextos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        xVisibleTexto01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        xVisibleTexto02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        xVisibleTexto03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        xVisibleTexto04 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        mBools[mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        xVisibleBool01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        xVisibleBool02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        xVisibleBool03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] += 1;
        mCaptionClass[xTipoDato::Numerico, mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pCaption;
        mDecs[mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pValorInicial;
        xVisibleDec01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 1;
        xVisibleDec02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 2;
        xVisibleDec03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Date)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xTipoDato::Fecha, mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        mDates[mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        xVisibleDate01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        xVisibleDate02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        xVisibleDate03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;

    procedure CampoF(pCaption: Text; pValorInicial: Text; pPassword: Boolean)
    begin
        if pPassword then begin
            mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] += 1;
            mCaptionClass[xTipoDato::Password, mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pCaption;
            mPass[mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pValorInicial;
            xVisiblePass01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 1;
            xVisiblePass02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 2;
            xVisiblePass03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 3;
        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;

    /// <summary>
    /// Devuelve el texto introducido por el usuario
    /// /// /// </summary>
    /// <returns>Return value of type Text begin.</returns>
    procedure CampoF(): Text
    var

    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(xTextos[mPunteros[xTipoPuntero::Leido, xTipoDato::Texto]]);
    end;


    procedure CampoF(var pSalida: Boolean): Boolean
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := mBools[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Decimal): Decimal
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico] += 1;
        pSalida := mDecs[mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico]];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Date): Date
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := mDates[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Text): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Password] += 1;
        pSalida := mPass[mPunteros[xTipoPuntero::Leido, xTipoDato::Password]];
        exit(pSalida);
    end;

    procedure CampoF(pPuntero: integer): Text
    var

    begin
        exit(xTextos[pPuntero]);
    end;


    procedure CampoF(var pSalida: Boolean; pPuntero: integer): Boolean
    begin
        pSalida := mBools[pPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Decimal; pPuntero: integer): Decimal
    begin
        pSalida := mDecs[pPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Date; pPuntero: integer): Date
    begin
        pSalida := mDates[pPuntero];
        exit(pSalida);
    end;

    procedure CampoF(var pSalida: Text; pPuntero: integer): Text
    begin
        pSalida := mPass[pPuntero];
        exit(pSalida);
    end;

}
