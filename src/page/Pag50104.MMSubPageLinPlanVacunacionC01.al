page 50104 "MMSubPageLinPlanVacunacionC01"
{
    Caption = 'Subpágina Líneas Plan Vacunación';
    PageType = ListPart;
    SourceTable = MMLinPlanVacunacionC01;
    ApplicationArea = All;
    UsageCategory = Lists;
    AutoSplitKey = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoLineas)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = False;
                }
                field("N_Linea"; Rec.N_Linea)
                {
                    ToolTip = 'Specifies the value of the Nº Línea field.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false;
                }
                field(Cliente; Rec.Cliente)
                {
                    ToolTip = 'Specifies the value of the Cliente field.';
                    ApplicationArea = All;

                }
                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    Caption = 'Fecha Vacuna';
                    ApplicationArea = All;

                }
                field(NombreCliente; Rec.NombreClienteF())
                {
                    Caption = 'Nombre Cliente';
                    ApplicationArea = All;
                    TableRelation = MMLinPlanVacunacionC01.Cliente;
                }
                field(TipoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo Vacuna field.';
                    ApplicationArea = All;
                    TableRelation = MMVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
                }
                field(TipoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo Otros field.';
                    ApplicationArea = All;
                    TableRelation = MMVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
                }
                field(DescripcionVacuna; Rec.DescripcionVariosF(rMMTipoC01.Tipo::Vacuna, Rec.CodigoVacuna))
                {
                    Caption = 'Descripción Vacuna';
                    ApplicationArea = All;
                }
                field(DescripcionOtros; Rec.DescripcionVariosF(eMMTipoC01::Otros, Rec.CodigoOtros))
                {
                    Caption = 'Descripción Otros';
                    ApplicationArea = All;
                }
                field(SiguienteFechaVacuna; Rec.SiguienteFechaVacuna)
                {
                    ToolTip = 'Specifies the value of the Siguiente Fecha Vacuna field.';
                    ApplicationArea = All;
                }
            }

        }
    }
    var
        rMMTipoC01: Record MMVariosC01;
        eMMTipoC01: Enum MMTipoVacunasC01;



    // trigger OnNewRecord(prRec: Boolean)
    // var
    //     rlCabPlanVacunacion: Record MMCabPlanVacunacionC01;
    // begin
    //     if rlCabPlanVacunacion.Get(Rec.CodigoLineas) then begin
    //         Rec.Validate(FechaVacunacion, rlCabPlanVacunacion.FechaVacunacionPlanificada);
    //     end;

    // end;

}
