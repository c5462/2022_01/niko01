page 50101 "MMListaVariosC01"
{
    ApplicationArea = All;
    Caption = 'Lista Tabla Varios';
    PageType = List;
    SourceTable = MMVariosC01;
    UsageCategory = Lists;
    Editable = false;
    AdditionalSearchTerms = 'Curso 01';
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    CardPageId = MMFichaVariosC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Specifies the value of the Bloqueado field.';
                    ApplicationArea = All;
                }
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                }
                field(PeriodoSegundaVacunacion; Rec.PeriodoSegundaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Periodo Segunda Vacunación field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
