page 50105 "MMListaCabPlanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'ListaCabPlanVacunacionC01';
    PageType = List;
    SourceTable = MMCabPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = MMFichaCabPlanVacunacionC01;
    Editable = false;
    ModifyAllowed = false;
    InsertAllowed = false;
    DeleteAllowed = false;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaVacunacionPlanificada; Rec.FechaVacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre empresa vacunadora';
                    ToolTip = 'Aquí aparecerá el nombre del proveedor que realizará la vacunación';
                    ApplicationArea = All;
                }

            }
        }
    }
    procedure GetSelectionFilterF(var xSalida: Record MMCabPlanVacunacionC01)  //De donde salen los campos que ha seleccionado
    begin
        CurrPage.SetSelectionFilter(xSalida); //Lo que ha seleccionado el usuario lo guarda en la variable xSalida.
    end;
}
