page 50103 "MMFichaCabPlanVacunacionC01"
{
    Caption = 'FichaCabPlanVacunacionC01';
    PageType = Document;
    SourceTable = MMCabPlanVacunacionC01;
    ApplicationArea = All;
    UsageCategory = Documents;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaVacunacionPlanificada; Rec.FechaVacunacionPlanificada)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Vacunacion Planificada field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre empresa vacunadora';
                    ApplicationArea = All;
                    ToolTip = 'Aquí aparecerá el nombre del proveedor que realizará la vacunación';
                }


            }
            part(Lineas; MMSubPageLinPlanVacunacionC01)
            {
                ApplicationArea = all;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
            }

        }




    }
    actions
    {
        area(Processing)
        {

            action(CrearLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas con Bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rlMMLinPlanVacunacionC01.Init();
                        rlMMLinPlanVacunacionC01.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlMMLinPlanVacunacionC01.Validate(N_Linea, i);
                        rlMMLinPlanVacunacionC01.Insert(true);
                        // if rlMMLinPlanVacunacionC01.N_Linea mod 2 = 0 then begin
                        //     rlMMLinPlanVacunacionC01.Validate(FechaVacunacion, Today);
                        //     rlMMLinPlanVacunacionC01.Modify(true);
                        // end;
                    end;
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;


            }

            action(CrearLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas Sin Bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10000 do begin
                        rlMMLinPlanVacunacionC01.Init();
                        rlMMLinPlanVacunacionC01.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlMMLinPlanVacunacionC01.Validate(N_Linea, i);
                        if not rlMMLinPlanVacunacionC01.Insert(true) then begin
                            Error('No se ha podido insertar la linea %1 del plan %2', rlMMLinPlanVacunacionC01.N_Linea, rlMMLinPlanVacunacionC01.CodigoLineas);
                        end;
                        // if rlMMLinPlanVacunacionC01.N_Linea mod 2 = 0 then begin
                        //     rlMMLinPlanVacunacionC01.Validate(FechaVacunacion, Today);
                        //     rlMMLinPlanVacunacionC01.Modify(true);
                        // end;
                    end;
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;


            }
            action(ModificarLineas)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas Sin Bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlMMLinPlanVacunacionC01.SetRange(rlMMLinPlanVacunacionC01.CodigoLineas, Rec.CodigoCabecera);
                    rlMMLinPlanVacunacionC01.ModifyAll(FechaVacunacion, Today, true);
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;


            }
            action(ModificarLineasNoBulk)
            {
                ApplicationArea = All;
                Caption = 'Crear Lineas Sin Bulk';
                Image = Create;
                trigger OnAction()
                var
                    rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
                    xlInicio: DateTime;

                begin
                    xlInicio := CurrentDateTime;
                    rlMMLinPlanVacunacionC01.SetRange(rlMMLinPlanVacunacionC01.CodigoLineas, Rec.CodigoCabecera);
                    if rlMMLinPlanVacunacionC01.FindSet(true, false) then begin //SEGUNDO PARAMETRO MODIFICAR FILTRO Y CLAVE
                        repeat
                            rlMMLinPlanVacunacionC01.Validate(FechaVacunacion, Today + 1);
                            rlMMLinPlanVacunacionC01.Modify(true);
                        until rlMMLinPlanVacunacionC01.Next() = 0;
                    end;
                    Message('Tiempo %1', CurrentDateTime - xlInicio);
                end;


            }

        }
    }

}
