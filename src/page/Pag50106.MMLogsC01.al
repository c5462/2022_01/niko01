page 50106 "MMLogsC01"
{
    ApplicationArea = All;
    Caption = 'LogsC01';
    PageType = List;
    SourceTable = MMLogC01;
    UsageCategory = History;
    InsertAllowed = false;
    DeleteAllowed = true;
    ModifyAllowed = false;
    SourceTableView = sorting(SystemCreatedAt) order(descending);


    layout
    {

        area(content)
        {
            repeater(General)
            {
                field(IdUnico; Rec.IdUnico)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
        }
    }

}
