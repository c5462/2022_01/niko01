table 50104 "MMLogC01"
{
    Caption = 'Tabla Log';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; IdUnico; Guid)
        {
            Caption = 'Id';
            DataClassification = SystemMetadata;

        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; IdUnico)
        {
            Clustered = true;
        }
        key(PK1; SystemCreatedAt)
        {
        }
    }
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.IdUnico) then begin
            Rec.Validate(IdUnico, CreateGuid());
        end;
    end;
}
