table 50103 "MMLinPlanVacunacionC01"
{
    Caption = 'Líneas Plan de Vacunación';
    DataClassification = ToBeClassified;
    LookupPageId = MMSubPageLinPlanVacunacionC01;
    DrillDownPageId = MMSubPageLinPlanVacunacionC01;
    fields
    {
        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(2; "N_Linea"; Integer)
        {
            Caption = 'Nº línea';
            DataClassification = SystemMetadata;
        }

        field(3; Cliente; Code[20])
        {
            Caption = 'Cliente';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            trigger OnValidate()
            var
                rlMMCabPlanVacunacion01: Record MMCabPlanVacunacionC01;
            begin
                if Rec.FechaVacunacion = 0D then begin
                    if rlMMCabPlanVacunacion01.Get(Rec.CodigoLineas) then begin
                        Rec.Validate(FechaVacunacion, rlMMCabPlanVacunacion01.FechaVacunacionPlanificada);
                    end;
                end;

            end;
        }
        field(4; CodigoVacuna; Code[20])
        {
            Caption = 'Tipo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = MMVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));
            trigger OnValidate()
            begin
                CalcularFechaProxVacunacionF();
            end;
        }
        field(5; CodigoOtros; Code[20])
        {
            Caption = 'Tipo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = MMVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
        }
        field(6; FechaVacunacion; Date)
        {
            Caption = 'FechaVacunacion';
            DataClassification = OrganizationIdentifiableInformation;
            trigger OnValidate()
            begin
                CalcularFechaProxVacunacionF();
            end;
        }
        field(7; SiguienteFechaVacuna; Date)
        {
            Caption = 'Siguiente Fecha Vacuna';
            DataClassification = SystemMetadata;

        }
    }
    keys
    {
        key(PK; CodigoLineas, N_Linea)
        {
            Clustered = true;
        }
    }
    procedure NombreClienteF(): Text
    var
        rlCliente: Record Customer; //Variable local de tipo Registro (rlXXX)
    begin
        if rlCliente.Get(Rec.Cliente) then begin
            exit(rlCliente.Name);
        end;
    end;

    procedure DescripcionVariosF(pTipo: Enum MMTipoVacunasC01; pCode: Code[20]): Text
    var
        rlVarios: Record MMVariosC01;
    begin
        if rlVarios.Get(pTipo, pCode) then begin
            exit(rlVarios.Descripcion);
        end;
    end;

    local procedure CalcularFechaProxVacunacionF()
    var
        rlMMVariosC01: Record MMVariosC01;
        IsHandled: Boolean;
    begin
        OnBeforeCalcularFechaProxVacunacionF(Rec, xRec, rlMMVariosC01, IsHandled);
        if not IsHandled then begin
            if Rec.FechaVacunacion <> 0D then begin
                if rlMMVariosC01.Get(rlMMVariosC01.Tipo::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.Validate(SiguienteFechaVacuna, CalcDate(rlMMVariosC01.PeriodoSegundaVacunacion, FechaVacunacion));
                end;
            end;
            OnAfterCalcularFechaProxVacunacionF(Rec, xRec, rlMMVariosC01);
        end;
    end;

    [BusinessEvent(false)]
    local procedure OnAfterCalcularFechaProxVacunacionF(Rec: Record MMLinPlanVacunacionC01; xRec: Record MMLinPlanVacunacionC01; rlMMVariosC01: Record MMVariosC01)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProxVacunacionF(Rec: Record MMLinPlanVacunacionC01; xRec: Record MMLinPlanVacunacionC01; rlMMVariosC01: Record MMVariosC01; var IsHandled: Boolean)
    begin
    end;


}
