table 50101 "MMVariosC01"
{
    Caption = 'Varios';
    LookupPageId = MMListaVariosC01;
    DrillDownPageId = MMFichaVariosC01;
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Tipo; Enum MMTipoVacunasC01)
        {
            Caption = 'Tipo';
            DataClassification = SystemMetadata;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = SystemMetadata;
        }
        field(4; Bloqueado; Boolean)
        {
            DataClassification = OrganizationIdentifiableInformation;
            Caption = 'Bloqueado';
        }
        field(5; PeriodoSegundaVacunacion; DateFormula)
        {
            Caption = 'Periodo Segunda Vacunación';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {

        }
    }
    trigger OnDelete()
    begin
        Rec.TestField(Bloqueado, true);
    end;

}
