table 50100 "MMConfiguracionC01"
{
    Caption = 'Configuración de la app';
    DataClassification = OrganizationIdentifiableInformation;


    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. de registro';
            DataClassification = SystemMetadata;
        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Código cliente web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No.";
            ValidateTableRelation = true;

        }
        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto registro';
            DataClassification = SystemMetadata;
        }
        field(10; IdPassword; Guid)
        {
            DataClassification = SystemMetadata;

        }
        field(11; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;

        }
        field(4; TipoDoc; Enum MMTipoDocC01)
        {
            Caption = 'Tipo documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre del cliente';
            FieldClass = FlowField;
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }
        field(6; TipoTabla; Enum MMTipoTablaC01)
        {
            Caption = 'Tipo de tabla';
            DataClassification = SystemMetadata;
            trigger OnValidate()
            begin
                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    Rec.Validate(CodTabla, '');
                end;

            end;
        }
        field(7; CodTabla; Code[20])
        {
            Caption = 'CodTabla';
            DataClassification = SystemMetadata;
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;
        }
        field(8; ColorFondo; Enum MMColorFondoC01)
        {
            Caption = 'ColorFondo';
            DataClassification = SystemMetadata;
        }

        field(9; ColorLetra; Enum MMColorFondoC01)
        {
            Caption = 'ColorLetra';
            DataClassification = SystemMetadata;
        }



    }


    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {

    }

    /// <summary>
    /// Recupera el registro de la tabla actual y si no existe lo crea.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;
    /// <summary>
    /// Devuelve el nombre del cliente del párametro pCodCte. Si no existe devolverá un texto vacío.
    /// </summary>
    /// <param name="pCodCte"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodCte: Code[20]): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(pCodCte) then begin
            exit(rlCustomer.Name);
        end;
    end;

    /// <summary>
    /// Devuelve el nombre correspondiente a los campos Tipo Tabla y Código Tabla seleccionados.
    /// </summary>
    /// <param name="pTipoTabla">Enum MMTipoTablaC01.</param>
    /// <param name="pCodTabla">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreTablaF(pTipoTabla: Enum MMTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlMMConfiguracionC01: Record MMConfiguracionC01;
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;
    begin
        case pTipoTabla of
            pTipoTabla::Cliente:
                begin
                    exit(rlMMConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.Get(pCodTabla) then begin
                        exit(rlContact.Name);
                    end;
                end;
            pTipoTabla::Empleado:
                begin
                    if rlEmployee.Get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlVendor.Get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlResource.Get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;


        end;
    end;

    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        Rec.Validate(IdPassword, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.IdPassword, pPassword, DataScope::Company);
    end;

    [NonDebuggable]
    procedure PasswordF() xSalida: Text
    begin
        if not IsolatedStorage.Get(Rec.IdPassword, DataScope::Company, xSalida) then begin
            Error('No se encuentra el password');
        end;
    end;
}
