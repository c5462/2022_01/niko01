table 50102 "MMCabPlanVacunacionC01"
{
    Caption = 'Cabecera Plan de Vacunación';
    DataClassification = ToBeClassified;
    LookupPageId = MMListaCabPlanVacunacionC01; //SAÑALA CUAL ES SU PÁGINA POR DEFECTO
    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = SystemMetadata;
        }
        field(3; FechaVacunacionPlanificada; Date)
        {
            Caption = 'Fecha Inicio Vacunación Planificada';
            DataClassification = SystemMetadata;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'EmpresaVacunadora';
            DataClassification = SystemMetadata;
            TableRelation = Vendor."No.";
        }

    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }
    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor; //Variable local de tipo Registro (rlXXX)
    begin
        if rlVendor.Get(Rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    var
        rlMMLinPlanVacunacionC01: Record MMLinPlanVacunacionC01;
    begin
        rlMMLinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
        rlMMLinPlanVacunacionC01.DeleteAll(true); //DELETEALL TEMPORAL FALSO
    end;

}
