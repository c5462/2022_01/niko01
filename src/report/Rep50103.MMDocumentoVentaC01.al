report 50103 "MMDocumentoVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    // DefaultLayout = RDLC;
    DefaultLayout = Word;
    RDLCLayout = 'documentoventa.rdlc';
    WordLayout = 'documentoventa.docx';

    dataset
    {
        dataitem(SalesHeader; "Sales Header")
        {
            RequestFilterFields = "No.", "Document Type";
            column(No_SalesHeader; "No.")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(xDocumentLabel; xDocumentLabel)
            {


            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }
            column(xCustAddr3; xCustAddr[3])
            {

            }
            column(xCustAddr4; xCustAddr[4])
            {

            }
            column(xCustAddr5; xCustAddr[5])
            {

            }
            column(xCustAddr6; xCustAddr[6])
            {

            }
            column(xCustAddr7; xCustAddr[7])
            {

            }
            column(xCustAddr8; xCustAddr[8])
            {

            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
            column(xTotalIva11; xTotalIva[1, 1])
            {

            }
            column(xTotalIva21; xTotalIva[2, 1])
            {

            }
            column(xTotalIva31; xTotalIva[3, 1])
            {

            }
            column(xTotalIva41; xTotalIva[4, 1])
            {

            }
            column(xTotalIva12; xTotalIva[1, 2])
            {

            }
            column(xTotalIva22; xTotalIva[2, 2])
            {

            }
            column(xTotalIva32; xTotalIva[3, 2])
            {

            }
            column(xTotalIva42; xTotalIva[4, 2])
            {

            }
            column(xTotalIva13; xTotalIva[1, 3])
            {

            }
            column(xTotalIva23; xTotalIva[2, 3])
            {

            }
            column(xTotalIva33; xTotalIva[3, 3])
            {

            }
            column(xTotalIva43; xTotalIva[4, 3])
            {

            }
            column(xTotalIva14; xTotalIva[1, 4])
            {

            }
            column(xTotalIva24; xTotalIva[2, 4])
            {

            }
            column(xTotalIva34; xTotalIva[3, 4])
            {

            }
            column(xTotalIva44; xTotalIva[4, 4])
            {

            }

            dataitem("Sales Line"; "Sales Line")
            {
                DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");

                column(No_SalesLine; "No.")
                {
                }
                column(Description_SalesLine; Description)
                {
                }
                column(Quantity_SalesLine; Quantity)
                {
                }
                column(UnitPrice_SalesLine; "Unit Price")
                {
                }
                column(LineDiscount_SalesLine; "Line Discount %")
                {
                }
                column(Amount_SalesLine; Amount)
                {
                }
                column(LineNo_SalesLine; "Line No.")
                {
                }


                dataitem("Extended Text Line"; "Extended Text Line")
                {
                    DataItemLink = "No." = field("No.");
                    column(Text_ExtendedTextLine; "Text")
                    {

                    }
                }
            }

            trigger OnAfterGetRecord()
            begin
                case SalesHeader."Document Type" of
                    salesheader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Oferta';
                        end;
                    salesheader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Pedido';
                        end;
                    salesheader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLabel := 'Nº Factura';
                        end;


                end;
                CalculoIva();


            end;

            trigger OnPreDataItem()
            begin
                SalesHeader.SetRange("Document Type", rSalesHeader."Document Type");
                SalesHeader.SetFilter("No.", xDocumentNo);
            end;
        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(rSalesHeader; rSalesHeader."Document Type")
                    {
                        ApplicationArea = All;

                    }
                    field(xDocumentNo; xDocumentNo)
                    {
                        ApplicationArea = All;
                        trigger OnLookup(var xDcoumentNo: Text): Boolean
                        var
                            plSalesHeader: Page "Sales List";
                        begin
                            rSalesHeader.SetRange("Document Type", rSalesHeader."Document Type"); //Tengo el campo como valor pero lo necesito como filtro
                            plSalesHeader.SetTableView(rSalesHeader); //aplico filtros de tabla a página
                            plSalesHeader.LookupMode := true; //pongo la página en modo LookUp (OK,CANCEL)

                            if plSalesHeader.RunModal() = Action::LookupOK then begin //Ejecuto página y controlo la acción del usuario

                                plSalesHeader.SetSelectionFilter(rSalesHeader); //Aplico selección del usuario de la página a la tabla
                                if rSalesHeader.FindSet() then begin //Inicialización de la búsqueda de la tabla
                                    repeat
                                        xDocumentNo += rSalesHeader."No." + '|'; //Elaboración de filtro
                                    until rSalesHeader.Next() = 0; //Puntero siguiente
                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|'); //Eliminar el último carácter de la cadena texto
                            end;
                        end;
                    }
                }
            }
        }

    }
    trigger OnPreReport()
    begin
        rCompanyInfo.get();
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
    end;

    procedure CalculoIva()
    var
        rlSalesLine: Record "Sales Line";
        rlTempVatAmount: Record "VAT Amount Line" temporary;
        i: Integer;
    begin
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");

        if rlSalesLine.FindSet() then begin
            repeat
                //Insert
                rlTempVatAmount.SetRange("VAT Identifier", rlSalesLine."VAT Identifier"); //rlSalesLine es la tabla que recorremos
                if rlTempVatAmount.FindFirst() then begin
                    rlTempVatAmount."VAT Base" += rlSalesLine.Amount;
                    rlTempVatAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVatAmount."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVatAmount."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVatAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVatAmount.Modify(false);
                end else begin
                    rlTempVatAmount.Init();
                    rlTempVatAmount."VAT Base" := rlSalesLine.Amount;
                    rlTempVatAmount."VAT %" := rlSalesLine."VAT %";
                    rlTempVatAmount."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVatAmount."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVatAmount."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVatAmount.Insert(false);
                end;           //Modify
            until rlSalesLine.Next() = 0;
        end;
        Clear(xTotalIva);
        rlTempVatAmount.FindSet();
        for i := 1 to 4 do begin
            xTotalIva[1, i] := Format(rlTempVatAmount."VAT Base");
            xTotalIva[2, i] := Format(rlTempVatAmount."VAT %");
            xTotalIva[3, i] := Format(rlTempVatAmount."VAT Amount");
            xTotalIva[4, i] := Format(rlTempVatAmount."Amount Including VAT");
            if rlTempVatAmount.Next() = 0 then begin
                i := 1000;
            end;
        end;

    end;

    var
        myInt: Integer;
        rSalesHeader: Record "Sales Header";
        xDocumentNo: text;
        xCustAddr: array[8] of Text;
        xCompanyAddr: array[8] of Text;
        xTotalIva: array[4, 4] of Text;
        cuFormatAddress: Codeunit "Format Address";
        rCompanyInfo: Record "Company Information";
        xDocumentLabel: text;
}