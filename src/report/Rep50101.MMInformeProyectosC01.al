report 50101 "MMInformeProyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'ListadoProyectos.rdlc';
    AdditionalSearchTerms = 'Listado Proyectos';
    Caption = 'Listado Proyectos';

    dataset
    {
        dataitem(Proyectos; Job)
        {
            // DataItemTableView = sorting("No.");
            RequestFilterFields = "No.", "Creation Date", Status;
            column(NoProyecto;
            "No.")
            {

            }

            column(ClienteAsociado; "Bill-to Customer No.")
            {

            }
            column(NombreCliente; "Bill-to Name")
            {

            }

            column(Descripcion; Description)
            {

            }

            column(FechaCreacion; Format("Creation Date"))
            {

            }

            column(Estado; Status)
            {

            }
            column(xColor; xColor)
            {

            }
            dataitem(Tareas; "Job Task")
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");
                column(NumTarea; "Job Task No.")
                {

                }

                column(DescripcionTarea; Description)
                {

                }
                //  column(DescriptionCaption; FieldCaption(Description))
                // {

                // }
                column(DiaComienzoTarea; Format("Start Date"))
                {

                }
                column(DiaFinalizacionTarea; Format("End Date"))
                {

                }
                column(JobTaskTareas; "Job Task No.")
                {
                }
                dataitem(LineasPlanificacion; "Job Planning Line")
                {
                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    column(No_LineasPlanificacion; "No.")
                    {
                    }
                    column(LineNo; "Line No.")
                    {

                    }
                    column(Description_LineasPlanificacion; Description)
                    {
                    }
                    column(Quantity_LineasPlanificacion; Quantity)
                    {
                    }
                    column(PlanningDate_LineasPlanificacion; Format("Planning Date"))
                    {
                    }
                    column(LineAmount_LineasPlanificacion; "Line Amount")
                    {
                    }
                    column(Type_LineasPlanificacion; "Type")
                    {
                    }
                    dataitem(MovProyectos; "Job Ledger Entry")
                    {

                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");
                        column(DocumentNo_MovProyectos;
                        "Document No.")
                        {
                        }
                        column(DocumentDate_MovProyectos; Format("Document Date"))
                        {
                        }
                        column(Type_MovProyectos; "Type")
                        {
                        }
                        column(EntryType_MovProyectos; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_MovProyectos; "Ledger Entry Type")
                        {
                        }
                        column(LineType_LineasPlanificacion; "Line Type")
                        {
                        }
                    }

                }



            }

            trigger OnAfterGetRecord()
            begin
                case Proyectos.Status of
                    Proyectos.Status::Completed:
                        begin
                            xColor := 'Green'
                        end;
                    Proyectos.Status::Open:
                        begin
                            xColor := 'Blue'
                        end;
                    Proyectos.Status::Planning:
                        begin
                            xColor := 'Yellow'
                        end;
                    Proyectos.Status::Quote:
                        begin
                            xColor := 'Red'
                        end;


                end;
            end;
        }

    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

    }

    var
        xColor: Text;
}