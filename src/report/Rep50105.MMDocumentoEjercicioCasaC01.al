report 50105 "MMDocumentoEjercicioCasaC01"
{
    Caption = 'Ejercicio para casa';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'EjercicioCasa.rdlc';
    dataset
    {
        dataitem(Customer; Customer)
        {
            RequestFilterFields = "Country/Region Code";
            column(Name_Customer; Name)
            {
            }
            column(PaymentMethodCode_Customer; "Payment Method Code")
            {
            }
            column(SalesLCY_Customer; "Sales (LCY)")
            {
            }
            column(PaymentsLCY_Customer; "Payments (LCY)")
            {
            }
            column(Balance_Customer; Balance)
            {
            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
            dataitem("Sales Header"; "Sales Header")
            {
                DataItemLink = "Sell-to Customer No." = field("No.");

                column(No_SalesHeader; "No.")
                {
                }
                column(Amount_SalesHeader; Amount)
                {
                }
                column(AmountIncludingVAT_SalesHeader; "Amount Including VAT")
                {
                }

            }
            dataitem(Vendor; Vendor)
            {

                RequestFilterFields = "Country/Region Code";
                column(Name_Vendor; Name)
                {
                }
                column(Address_Vendor; Address)
                {
                }
                column(CreditAmount_Vendor; "Credit Amount")
                {
                }
                dataitem("Purchase Header"; "Purchase Header")
                {
                    DataItemLink = "Buy-from Vendor No." = field("No.");
                    column(No_PurchaseHeader; "No.")
                    {
                    }
                    column(PostingDate_PurchaseHeader; "Posting Date")
                    {
                    }
                    column(Amount_PurchaseHeader; Amount)
                    {
                    }
                    column(AmountIncludingVAT_PurchaseHeader; "Amount Including VAT")
                    {
                    }
                }
            }
        }
    }
    trigger OnPreReport()
    begin

        rCompanyInfo.get();
        rCompanyInfo.CalcFields(Picture);
        cuFormatAdress.Company(xCompanyAddr, rCompanyInfo);

    end;

    var
        xCompanyAddr: array[8] of Text;
        rCompanyInfo: Record "Company Information";
        cuFormatAdress: Codeunit "Format Address";

}
