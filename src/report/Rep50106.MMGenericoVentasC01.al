report 50106 "MMGenericoVentasC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    RDLCLayout = 'genericoventas.rdlc';
    DefaultLayout = RDLC;
    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    MMLinGenericaVentasC01.Init();
                    MMLinGenericaVentasC01.TransferFields("Sales Invoice Line");
                    MMLinGenericaVentasC01.Insert(false);
                end;
            }
            trigger OnPreDataItem()
            begin
                if "Sales Invoice Header".GetFilters() = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                MMLinGenericaVentasC01.Init();
                MMLinGenericaVentasC01.TransferFields("Sales Invoice Header");
                MMLinGenericaVentasC01.Insert(false);
                xCaptionInforme := 'Factura';
            end;

        }
        dataitem("Sales Shipment Header"; "Sales Shipment Header")
        {
            RequestFilterFields = "No.";
            dataitem("Sales Shipment Line"; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = field("No.");
                trigger OnAfterGetRecord()
                begin
                    MMLinGenericaVentasC01.Init();
                    MMLinGenericaVentasC01.TransferFields("Sales Shipment Line");
                    MMLinGenericaVentasC01.Insert(false);
                end;
            }
            trigger OnPreDataItem()
            begin
                if "Sales Shipment Header".GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                MMLinGenericaVentasC01.Init();
                MMLinGenericaVentasC01.TransferFields("Sales Shipment Header");
                MMLinGenericaVentasC01.Insert(false);

                xCaptionInforme := 'Albaran';
            end;

        }
        dataitem(MMCabeceraGenericaVentasC01; MMCabeceraGenericaVentasC01)
        {
            RequestFilterFields = "No.";
            column(No_MMCabeceraGenericaVentasC01; "No.")
            {
            }
            column(SelltoCustomerNo_MMCabeceraGenericaVentasC01; "Sell-to Customer No.")
            {
            }
            column(PostingDate_MMCabeceraGenericaVentasC01; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {

            }
            dataitem(MMLinGenericaVentasC01; MMLinGenericaVentasC01)
            {
                DataItemLink = "Document No." = field("No.");
                column(No_MMLinGenericaVentasC01; "No.")
                {
                }
                column(Description_MMLinGenericaVentasC01; Description)
                {
                }
                column(Quantity_MMLinGenericaVentasC01; Quantity)
                {
                }
                trigger OnAfterGetRecord()
                begin
                    MMLinGenericaVentasC01.Init();
                    MMLinGenericaVentasC01.TransferFields("Sales Shipment Line");
                    MMLinGenericaVentasC01.Insert(false);
                end;
            }
            trigger OnAfterGetRecord()
            begin
                MMLinGenericaVentasC01.Init();
                MMLinGenericaVentasC01.TransferFields("Sales Shipment Header");
                MMLinGenericaVentasC01.Insert(false);
            end;

        }
    }



    var
        myInt: Integer;
        xCaptionInforme: Text;
}