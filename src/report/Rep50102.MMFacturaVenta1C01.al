report 50102 "MMFacturaVenta1C01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'FacturaVenta1.rdlc';

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {
            column(No_SalesInvoiceHeader; "No.")
            {

            }
            column(xCustAddr1; xcustAddr[1])
            {

            }
            column(xCustAddr2; xcustAddr[2])
            {

            }
            column(xShipAddr1; xShipAddr[2])
            {

            }
            column(xShipAddr2; xShipAddr[2])
            {

            }
            dataitem(Integer; Integer)
            {
                column(Number; Number)
                {

                }
                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {

                    column(LineNo_SalesInvoiceLine; "Line No.")
                    {
                    }
                    trigger OnPreDataItem()
                    begin
                        "Sales Invoice Line".SetRange("Document No.", "Sales Invoice Header"."No.");
                    end;
                }
                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, xCopias);
                end;
            }
            trigger OnAfterGetRecord()
            begin
                cuFormatAdress.SalesInvSellTo(xCustAddr, "Sales Invoice Header");
                cuFormatAdress.SalesInvShipTo(xShipAddr, xCustAddr, "Sales Invoice Header");
                rCompanyInfo.Get;
                cuFormatAdress.Company(xCompanyAddr, rCompanyInfo);
            end;
        }
    }


    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xCopias; xCopias)
                    {
                        ApplicationArea = All;
                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;
        cuFormatAdress: Codeunit "Format Address";
        xCopias: Integer;

        xCompanyAddr: array[8] of Text;
        rCompanyInfo: Record "Company Information";
}