report 50100 "MMListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = 'ListadoProductos.rdl';
    AdditionalSearchTerms = 'Listado Productos Ventas/Compras';
    Caption = 'Listado Productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item)
        {
            RequestFilterFields = "No.", "Item Category Code";
            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                RequestFilterFields = "Posting Date";
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                // DataItemLink = "No." = field("No."); //field del dataitem superior

                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(NoCliente; rCustomer."No.")
                {

                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }

                trigger OnPreDataItem()
                begin
                    FacturaVenta.SetRange("No.", Productos."No.");

                end;

                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin //AQUÍ RELACIONA
                        Clear(rCustomer);
                    end;
                    //setRange or setFilter
                    // rCustomer.SetRange("No.", FacturaVenta."Sell-to Customer No."); //AQUÍ FILTRA CLIENTES QUE HAY EN LA TABLA DE FACTURAVENTA
                    // rCustomer.SetFilter("No.", FacturaVenta."Sell-to Customer No." + '&' + FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();
                end;
            }

            dataitem(FacturaCompra; "Purch. Inv. Line")
            {

                RequestFilterFields = "Posting Date";
                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(NombreProveedor; rVendor.Name)
                {

                }
                column(NoProveedor; rVendor."No.")
                {

                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                trigger OnPreDataItem()
                begin
                    FacturaCompra.SetRange("No.", Productos."No.");

                end;

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.Get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;

                end;
            }

        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }

    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}