reportextension 50100 "MMExtensionEjerCasaC01" extends "Sales - Shipment"
{
    dataset
    {
        addlast("Sales Shipment Header")
        {
            dataitem("MMSalesShipmentHeader2"; "Sales Shipment Header")
            {

                column(MMShiptoCity_MMSalesShipmentHeader2; "Ship-to City")
                {
                }
                column(MMShiptoCode_MMSalesShipmentHeader2; "Ship-to Code")
                {
                }
                column(MMShiptoName_MMSalesShipmentHeader2; "Ship-to Name")
                {
                }
            }
        }
    }
}